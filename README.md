# RT-Thread环境下TPM模拟器移植

## 简介

包括国密算法sm2、sm3、sm4，模拟机cube相关支持的功能字库，tpm2模拟器相关命令函数支持

## 硬件说明
<img src="./figures/blink_pcb.png" alt="LED 连接单片机引脚" style="zoom: 50%;" />

如上图所示，RGB-LED 属于共阳 LED， **阴极** 分别与单片机的引脚相连，其中蓝色 LED 对应 PI8 引脚。单片机引脚输出低电平即可点亮 LED，输出高电平则会熄灭 LED。

## 软件说明

国密算法SM2 在 gm_sm2_master文件夹中，如图所示
<img src="./figures/SM2.jpg" alt="国密算法SM2" style="zoom: 50%;" />

国密算法SM3 在 gm_sm3_master文件夹中，如图所示
<img src="./figures/SM3.jpg" alt="国密算法SM2" style="zoom: 50%;" />

国密算法SM4 在 gm_sm4_master文件夹中，如图所示
<img src="./figures/SM4.jpg" alt="国密算法SM2" style="zoom: 50%;" />

cube模拟器相关支持 在 cube文件夹中，如图所示
<img src="./figures/cube.jpg" alt="国密算法SM2" style="zoom: 50%;" />

tpm2模拟器相关命令函数支持 在 cube-tpm2文件夹中，如图所示
<img src="./figures/cube-tpm2.jpg" alt="国密算法SM2" style="zoom: 50%;" />

在每个文件夹中都有各自模块的test文件，在每个test文件中都会使用 ` MSH_CMD_EXPORT `命令将函数预置到msh命令中，可根据自己调用不同命令。

在 main 函数中，会打印提示信息。

```
int main()
{
    printf("hello,rtthread!\n");
    printf("Welcome to use GM algorithm and TPM2 simulator!\n");

    return 0;
}
```



## 运行
### 编译&下载

编译完成后，将开发板的 ST-Link USB 口与 PC 机连接，然后将固件下载至开发板。

### 运行效果

正常运行后，会出现` msh-> `命令输入提示，可根据不同命令输出不同内容。

## 注意事项

部分文件可能需要在外置sd卡中存在，需要插入sd卡并写入相关文件。

