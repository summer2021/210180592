/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-08-13     han       the first version
 */

#include <dfs_posix.h>
#include <rtthread.h>
#include <rtdevice.h>
#include "drv_common.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "dfs_fs.h"

#define LED_PIN GET_PIN(I, 8)

int cubeTest(void)
{
    rt_uint32_t count = 1;
//    rt_kprintf("call alloc test func\n");
//    test_alloc_main();
//    rt_kprintf("call string test func\n");
//    test_string_main();
//    rt_kprintf("call basefunc func\n");
//    test_basefunc_main();
//    rt_kprintf("call json func\n");
//    test_json_main();

    rt_pin_mode(LED_PIN, PIN_MODE_OUTPUT);

    while (count++)
    {
        rt_thread_mdelay(500);
        rt_pin_write(LED_PIN, PIN_HIGH);
        rt_thread_mdelay(500);
        rt_pin_write(LED_PIN, PIN_LOW);
    }
    return RT_EOK;
}

int main_test_read_json_file()
{
    int ret;
    char *file_name = "headrecord.json";
//    char *file_name = "tpm2_general_define.json";

    int fd;
    int readlen;
    int json_offset;

    int struct_no = 0;
    void *root_node;
    void *findlist;
    void *memdb_template;
    unsigned char uuid[32];
//    char json_buffer[140960];
    char * json_buffer;
    json_buffer = rt_malloc(140960);
//    memset(json_buffer,0,4096);
    fd = open(file_name, O_RDONLY);
    if (fd < 0)
        return fd;

    readlen = read(fd, json_buffer, 140960);
    if (readlen < 0)
        return -EIO;
    json_buffer[readlen] = 0;
    printf("%s\n", json_buffer);
    close(fd);

    json_offset = 0;
    while (json_offset < readlen)
    {
        ret = json_solve_str(&root_node, json_buffer + json_offset);
        if (ret < 0)
        {
            rt_kprintf("solve json str error!\n");
            break;
        }
        json_offset += ret;
        if (ret < 32)
            continue;

        ret = memdb_read_desc(root_node, uuid);
        if (ret < 0)
            break;
        struct_no++;
    }

    return struct_no;
}


int main()
{
    rt_kprintf("hello,rtthread!\n");
    rt_kprintf("Welcome to use GM algorithm and TPM2 simulator!\n");
//    cubeTest();
//    main_test_read_json_file();
//    main_test_read_json_file("tpm2_value.json");
    return 0;
}

MSH_CMD_EXPORT(cubeTest, cubeTest)
MSH_CMD_EXPORT(main_test_read_json_file, main_test_read_json_file)

#include "stm32h7xx.h"
static int vtor_config(void)
{
    /* Vector Table Relocation in Internal QSPI_FLASH */
    SCB->VTOR = QSPI_BASE;
    return 0;
}
INIT_BOARD_EXPORT(vtor_config);

