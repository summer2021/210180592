/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-08-13     han       the first version
 */

#include <rtthread.h>
#include "data_type.h"

// 测试 sm4 算法
int sm4test()
{

    int i = 0;
    char input[16];
    char *output;
    char *decrypt;
    for (i = 0; i < sizeof(input); ++i)
    {
        input[i] = rand() % (0xff);
    }
    printf("message is \n");
    for (i = 0; i < sizeof(input); ++i)
    {
        printf("%02x ", input[i]);
    }
    printf("\n");
    printf("passwd is \n");
    char passwd[16];
    for (i = 0; i < sizeof(passwd); ++i)
    {
        passwd[i] = rand() % (0xff);
        printf("%02x ", passwd[i]);
    }
    printf("\n");
    int cryptlength = 0;
    cryptlength = sm4_context_crypt(input, &output, sizeof(input), passwd);
    printf("cryptlength is %d\n", cryptlength);
    printf("crypt message is \n");
    for (i = 0; i < sizeof(output); ++i)
    {
        printf("%02x ", output[i]);
    }
    printf("\n");
    sm4_context_decrypt(output, &decrypt, cryptlength, passwd);
    printf("decrypt message is\n ");
    for (i = 0; i < cryptlength; ++i)
    {
        printf("%02x ", decrypt[i]);
    }
    printf("\n");

}

MSH_CMD_EXPORT(sm4test, sm4test)
