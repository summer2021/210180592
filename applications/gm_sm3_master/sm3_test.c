/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-08-13     han       the first version
 */

#include <rtthread.h>
#include "data_type.h"

// 测试 sm3 算法
int sm3exttest()
{

    // 固定值测试
    char input[3] = { 'a', 'b', 'c' };
    unsigned char output[32];
    sm3_ext(input, sizeof(input), output);
    int i = 0;
    printf("input : \n");
    for (i = 0; i < sizeof(input); ++i)
    {
        printf("%02X ", input[i]);
    }
    printf("\n");
    printf("output : \n");
    for (i = 0; i < sizeof(output); i++)
    {
        printf("%02X ", output[i]);
    }
    printf("\n");

    // 随机值 测试
    printf("passwd is: \n");
    char key[32];
    for (i = 0; i < 32; ++i)
    {
        key[i] = rand() % (0xff);
        printf("%02x ", key[i]);
    }
    printf("\n");
    SM3_hmac(key, sizeof(key), input, sizeof(input), output);
    printf("output : \n");
    for (i = 0; i < sizeof(output); i++)
    {
        printf("%02X ", output[i]);
    }
    printf("\n");
    return 0;
}

MSH_CMD_EXPORT(sm3exttest, sm3exttest)
