#ifndef  CUBE_BASEFUNC_H
#define  CUBE_BASEFUNC_H

// pointer stack function

typedef struct uuid_head
{
	BYTE uuid[DIGEST_SIZE];
	char name[DIGEST_SIZE];
	int type;
	int subtype;
}__attribute__((packed)) UUID_HEAD;

void * init_hash_list(int order,int type,int subtype);
void * hashlist_get_desc(void * hashlist);
int  hashlist_set_desc(void * hashlist,void * desc);
void *  hashlist_add_elem(void * hashlist,void * elem);
void * hashlist_find_elem(void * hashlist,void * elem);
void * hashlist_find_elem_byname(void * hashlist,char * name);
void * hashlist_remove_elem(void * hashlist,void * elem);
void * hashlist_get_first(void * hashlist);
void * hashlist_get_next(void * hashlist);


void * init_list_queue(void );
void free_list_queue(void * queue);
int  list_queue_put(void * list_queue,void * record);
int  list_queue_get(void * list_queue,void ** record);
void * list_queue_getfirst(void * list_queue);
void * list_queue_getnext(void * list_queue);
void * list_queue_removecurr(void * list_queue);
#endif
