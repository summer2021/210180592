#ifndef  CUBE_ALLOC_H
#define  CUBE_ALLOC_H

int alloc_init(void * start_addr,int page_num);
// init 

void * get_page_addr();

void free_page(void * page);

void * bmalloc(int size);
void * bmalloc0(int size);
void * Talloc(int size);
void * Talloc0(int size);
int TFree(void * block);

void * Salloc(int size);
void * Calloc(int size);
int CFree(void * addr);
int getfreecount(int type);

void * Dalloc(int size,void * parent);
void * Dalloc0(int size,void * parent);
void * Dpointer_set(void * pointer, void * base);
int DFree(void * addr);
void * Malloc(int size, void * pointer);


int Free(void * pointer);
int Free0(void * pointer);
void Cmemdestroy(void);
void Dmemdestroy(void);
void Tmemdestroy(void);
#endif
