#ifndef VTCM_ALG_H
#define VTCM_ALG_H

// 使用sm3算法，计算hash值
int vtcm_sm3(
    char *hashout,  // hash 计算输出值
    int elem_no,    // 共计算多少个hash
    ...             // 计算elem_no个hash对：指针，长度
);

int vtcm_Random(BYTE* buffer, size_t bytes);

// 使用sm3算法，计算hmac摘要值，比vtcm_sm3算法，多了一个密钥key，和密钥长度keylen
int vtcm_hmac_sm3(
    char *hashout,  // hash 计算输出值
    BYTE *key,      // 密钥key
    int keylen,     // 密钥长度keylen
    int elem_no,    // 共计算多少个hash
    ...             // 计算elem_no个hash对：指针，长度
);


int vtcm_ex_sm3(char * hashout,int elem_no,...);

int vtcm_ex_Random(BYTE* buffer, size_t bytes);

int vtcm_ex_hmac_sm3(char * hashout,BYTE * key, int keylen,int elem_no,...);

#endif
