#ifndef TPM2_SCRIPT_H
#define TPM2_SCRIPT_H

enum  dtype_tpm2_script
{
    TYPE(TPM2_SCRIPT)=0x2A00,
    TYPE(TPM2_SYS)=0x2A01,
    TYPE(TPM2_FILETRANS)=0x2A02
};

enum subtype_tpm2_script
{
	SUBTYPE(TPM2_SCRIPT,CALL)=0x01,
	SUBTYPE(TPM2_SCRIPT,RET)
};

enum subtype_tpm2_sys
{
	SUBTYPE(TPM2_SYS,CP),
	SUBTYPE(TPM2_SYS,MV),
	SUBTYPE(TPM2_SYS,RM),
	SUBTYPE(TPM2_SYS,RENAME)
};

enum  subtype_tpm2_filetrans
{
    SUBTYPE(TPM2_FILETRANS,SEND),
    SUBTYPE(TPM2_FILETRANS,RECEIVE)
};

typedef struct tpm2_script_call
{
    char name[DIGEST_SIZE];	
    int  param_num;
    BYTE * params;
}__attribute__((packed)) RECORD(TPM2_SCRIPT,CALL);

typedef struct tpm2_script_ret
{
    int returnCode;	
    int  param_num;
    BYTE * params;
    int cmd_no;
}__attribute__((packed)) RECORD(TPM2_SCRIPT,RET);
#endif
