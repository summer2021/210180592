/********************************************************************************/
/********************************************************************************/

#ifndef TPM2_VALUE_H
#define TPM2_VALUE_H

//#include "tcm_types.h"
//#include "tcm_structures.h"
#include "TpmProfile.h"


#define	ENOMEM		12	/* Out of memory */
#define	EINVAL		22	/* Invalid argument */


#ifndef IMPLEMENTATION_PCR
#define IMPLEMENTATION_PCR              24
#endif

#undef SET
#define SET                 1
#undef CLEAR
#define CLEAR               0

#define  USE_BIT_FIELD_STRUCTURES    1  

#ifndef MAX_LOADED_OBJECTS
#define MAX_LOADED_OBJECTS              3
#endif


#ifndef SM3_256_DIGEST_SIZE
#define SM3_256_DIGEST_SIZE             32
#endif

enum tpm2_st_value
{
	TPM_ST_NULL=0x0008,
	TPM_ST_NO_SESSIONS=0x1008,
	TPM_ST_SESSIONS=0x2008,
	TPM_ST_RSP_COMMAND=0xC400,
    TPM_ST_CREATION=0X2180
};

enum tpm2_cc_value
{
	TPM_CC_Create=0x53010000,
	TPM_CC_Load=0x57010000,
	TPM_CC_CreatePrimary=0x31010000,
	TPM_CC_GetRandom=0x7B010000,
	TPM_CC_PCR_Read=0x7E010000,
	TPM_CC_PCR_Extend=0x82010000,
	TPM_CC_PCR_SetAuthValue=0x83010000
};

enum tpm_alg_id
{
	TPM_ALG_SM3_256=0x0012,
	TPM_ALG_SM4=0x0013,
	TPM_ALG_SM2=0x001B,
	TPM_ALG_ECC=0x0023,
	TPM_ALG_NULL=0x0010
    
};

enum tpmi_rh_hierrchy
{
	TPM_RH_OWNER = 0x01000040,
	TPM_RH_PLATFORM = 0x0C000040,
	TPM_RH_ENDORSEMENT = 0x0B000040,
	TPM_RH_NULL = 0x07000040
};

enum tpma_locality
{
	TPM_LOC_ZERO = 0,
	TPM_LOC_ONE = 1,
	TPM_LOC_TWO = 2,
	TPM_LOC_THREE = 3,
	TPM_LOC_FOUR = 4
};


/* Capabilities.h */
#ifndef     _CAPABILITIES_H
#define     _CAPABILITIES_H


#define    MAX_CAP_DATA         (MAX_CAP_BUFFER - sizeof(TPM_CAP)-sizeof(UINT32))
#define    MAX_CAP_ALGS         (MAX_CAP_DATA / sizeof(TPMS_ALG_PROPERTY))
#define    MAX_CAP_HANDLES      (MAX_CAP_DATA / sizeof(TPM_HANDLE))
#define    MAX_CAP_CC           (MAX_CAP_DATA / sizeof(TPM_CC))
#define    MAX_TPM_PROPERTIES   (MAX_CAP_DATA / sizeof(TPMS_TAGGED_PROPERTY))
#define    MAX_PCR_PROPERTIES   (MAX_CAP_DATA / sizeof(TPMS_TAGGED_PCR_SELECT))
#define    MAX_ECC_CURVES       (MAX_CAP_DATA / sizeof(TPM_ECC_CURVE))
#define    MAX_TAGGED_POLICIES  (MAX_CAP_DATA / sizeof(TPMS_TAGGED_POLICY))
#define    MAX_ACT_DATA		(MAX_CAP_DATA / sizeof(TPMS_ACT_DATA))
#define    MAX_AC_CAPABILITIES  (MAX_CAP_DATA / sizeof(TPMS_AC_OUTPUT))

#endif


/* Capabilities.h */

/* Table 1:2 - Definition of TPM_ALG_ID Constants */
typedef UINT16 TPM_ALG_ID;
#define     ALG_HMAC_VALUE              0x0005
#define TPM_ALG_HMAC                    (TPM_ALG_ID)(ALG_HMAC_VALUE)
#define     ALG_AES_VALUE               0x0006
#define TPM_ALG_AES                     (TPM_ALG_ID)(ALG_AES_VALUE)
#define     ALG_KEYEDHASH_VALUE         0x0008
#define TPM_ALG_KEYEDHASH               (TPM_ALG_ID)(ALG_KEYEDHASH_VALUE)
#define     ALG_XOR_VALUE               0x000A
#define TPM_ALG_XOR                     (TPM_ALG_ID)(ALG_XOR_VALUE)
// #define     ALG_SM3_256_VALUE           0x0012
// #define TPM_ALG_SM3_256                 (TPM_ALG_ID)(ALG_SM3_256_VALUE)
#define     ALG_KDF1_SP800_56A_VALUE    0x0020
#define TPM_ALG_KDF1_SP800_56A          (TPM_ALG_ID)(ALG_KDF1_SP800_56A_VALUE)
#define     ALG_KDF1_SP800_108_VALUE    0x0022
#define TPM_ALG_KDF1_SP800_108          (TPM_ALG_ID)(ALG_KDF1_SP800_108_VALUE)
#define     ALG_SYMCIPHER_VALUE         0x0025
#define TPM_ALG_SYMCIPHER               (TPM_ALG_ID)(ALG_SYMCIPHER_VALUE)
/* Table 1:3 - Definition of TPM_ECC_CURVE Constants */
typedef UINT16 TPM_ECC_CURVE;

#ifndef TPM_ECC_SM2_P256
#define TPM_ECC_SM2_P256    (TPM_ECC_CURVE)(0x0020)
#endif

/* Values derived from Table 1:2 */
#define     ALG_FIRST_VALUE             0x0001
#define TPM_ALG_FIRST                   (TPM_ALG_ID)(ALG_FIRST_VALUE)
#define     ALG_LAST_VALUE              0x0044
#define TPM_ALG_LAST                    (TPM_ALG_ID)(ALG_LAST_VALUE)
/* Table 2:7 - Definition of TPM_GENERATED Constants */

typedef UINT32                  TPM_GENERATED;
#define TYPE_OF_TPM_GENERATED   UINT32
#define TPM_GENERATED_VALUE     (TPM_GENERATED)(0xFF544347)

/* Table 2:12 - Definition of TPM_CC Constants */
typedef UINT32                              TPM_CC;
#define TYPE_OF_TPM_CC                      UINT32
#define TPM_CC_NV_UndefineSpaceSpecial      (TPM_CC)(0x0000011F)
#define TPM_CC_EvictControl                 (TPM_CC)(0x00000120)
#define TPM_CC_HierarchyControl             (TPM_CC)(0x00000121)
#define TPM_CC_NV_UndefineSpace             (TPM_CC)(0x00000122)
#define TPM_CC_ChangeEPS                    (TPM_CC)(0x00000124)
#define TPM_CC_ChangePPS                    (TPM_CC)(0x00000125)
#define TPM_CC_Clear                        (TPM_CC)(0x00000126)
#define TPM_CC_ClearControl                 (TPM_CC)(0x00000127)
#define TPM_CC_ClockSet                     (TPM_CC)(0x00000128)
#define TPM_CC_HierarchyChangeAuth          (TPM_CC)(0x00000129)
#define TPM_CC_NV_DefineSpace               (TPM_CC)(0x0000012A)
#define TPM_CC_PCR_Allocate                 (TPM_CC)(0x0000012B)
#define TPM_CC_PCR_SetAuthPolicy            (TPM_CC)(0x0000012C)
#define TPM_CC_PP_Commands                  (TPM_CC)(0x0000012D)
#define TPM_CC_SetPrimaryPolicy             (TPM_CC)(0x0000012E)
#define TPM_CC_FieldUpgradeStart            (TPM_CC)(0x0000012F)
#define TPM_CC_ClockRateAdjust              (TPM_CC)(0x00000130)
#define TPM_CC_CreatePrimary                (TPM_CC)(0x00000131)
#define TPM_CC_NV_GlobalWriteLock           (TPM_CC)(0x00000132)
#define TPM_CC_GetCommandAuditDigest        (TPM_CC)(0x00000133)
#define TPM_CC_NV_Increment                 (TPM_CC)(0x00000134)
#define TPM_CC_NV_SetBits                   (TPM_CC)(0x00000135)
#define TPM_CC_NV_Extend                    (TPM_CC)(0x00000136)
#define TPM_CC_NV_Write                     (TPM_CC)(0x00000137)
#define TPM_CC_NV_WriteLock                 (TPM_CC)(0x00000138)
#define TPM_CC_DictionaryAttackLockReset    (TPM_CC)(0x00000139)
#define TPM_CC_DictionaryAttackParameters   (TPM_CC)(0x0000013A)
#define TPM_CC_NV_ChangeAuth                (TPM_CC)(0x0000013B)
#define TPM_CC_PCR_Event                    (TPM_CC)(0x0000013C)
#define TPM_CC_PCR_Reset                    (TPM_CC)(0x0000013D)
#define TPM_CC_SequenceComplete             (TPM_CC)(0x0000013E)
#define TPM_CC_SetAlgorithmSet              (TPM_CC)(0x0000013F)
#define TPM_CC_SetCommandCodeAuditStatus    (TPM_CC)(0x00000140)
#define TPM_CC_FieldUpgradeData             (TPM_CC)(0x00000141)
#define TPM_CC_IncrementalSelfTest          (TPM_CC)(0x00000142)
#define TPM_CC_SelfTest                     (TPM_CC)(0x00000143)
#define TPM_CC_Startup                      (TPM_CC)(0x00000144)
#define TPM_CC_Shutdown                     (TPM_CC)(0x00000145)
#define TPM_CC_StirRandom                   (TPM_CC)(0x00000146)
#define TPM_CC_ActivateCredential           (TPM_CC)(0x00000147)
#define TPM_CC_Certify                      (TPM_CC)(0x00000148)
#define TPM_CC_PolicyNV                     (TPM_CC)(0x00000149)
#define TPM_CC_CertifyCreation              (TPM_CC)(0x0000014A)
#define TPM_CC_Duplicate                    (TPM_CC)(0x0000014B)
#define TPM_CC_GetTime                      (TPM_CC)(0x0000014C)
#define TPM_CC_GetSessionAuditDigest        (TPM_CC)(0x0000014D)
#define TPM_CC_NV_Read                      (TPM_CC)(0x0000014E)
#define TPM_CC_NV_ReadLock                  (TPM_CC)(0x0000014F)
#define TPM_CC_ObjectChangeAuth             (TPM_CC)(0x00000150)
#define TPM_CC_PolicySecret                 (TPM_CC)(0x00000151)
#define TPM_CC_Rewrap                       (TPM_CC)(0x00000152)
#define TPM_CC_Create                       (TPM_CC)(0x00000153)
#define TPM_CC_ECDH_ZGen                    (TPM_CC)(0x00000154)
#define TPM_CC_HMAC                         (TPM_CC)(0x00000155)
#define TPM_CC_MAC                          (TPM_CC)(0x00000155)
#define TPM_CC_Import                       (TPM_CC)(0x00000156)
#define TPM_CC_Load                         (TPM_CC)(0x00000157)
#define TPM_CC_Quote                        (TPM_CC)(0x00000158)
#define TPM_CC_RSA_Decrypt                  (TPM_CC)(0x00000159)
#define TPM_CC_HMAC_Start                   (TPM_CC)(0x0000015B)
#define TPM_CC_MAC_Start                    (TPM_CC)(0x0000015B)
#define TPM_CC_SequenceUpdate               (TPM_CC)(0x0000015C)
#define TPM_CC_Sign                         (TPM_CC)(0x0000015D)
#define TPM_CC_Unseal                       (TPM_CC)(0x0000015E)
#define TPM_CC_PolicySigned                 (TPM_CC)(0x00000160)
#define TPM_CC_ContextLoad                  (TPM_CC)(0x00000161)
#define TPM_CC_ContextSave                  (TPM_CC)(0x00000162)
#define TPM_CC_ECDH_KeyGen                  (TPM_CC)(0x00000163)
#define TPM_CC_EncryptDecrypt               (TPM_CC)(0x00000164)
#define TPM_CC_FlushContext                 (TPM_CC)(0x00000165)
#define TPM_CC_LoadExternal                 (TPM_CC)(0x00000167)
#define TPM_CC_MakeCredential               (TPM_CC)(0x00000168)
#define TPM_CC_NV_ReadPublic                (TPM_CC)(0x00000169)
#define TPM_CC_PolicyAuthorize              (TPM_CC)(0x0000016A)
#define TPM_CC_PolicyAuthValue              (TPM_CC)(0x0000016B)
#define TPM_CC_PolicyCommandCode            (TPM_CC)(0x0000016C)
#define TPM_CC_PolicyCounterTimer           (TPM_CC)(0x0000016D)
#define TPM_CC_PolicyCpHash                 (TPM_CC)(0x0000016E)
#define TPM_CC_PolicyLocality               (TPM_CC)(0x0000016F)
#define TPM_CC_PolicyNameHash               (TPM_CC)(0x00000170)
#define TPM_CC_PolicyOR                     (TPM_CC)(0x00000171)
#define TPM_CC_PolicyTicket                 (TPM_CC)(0x00000172)
#define TPM_CC_ReadPublic                   (TPM_CC)(0x00000173)
#define TPM_CC_RSA_Encrypt                  (TPM_CC)(0x00000174)
#define TPM_CC_StartAuthSession             (TPM_CC)(0x00000176)
#define TPM_CC_VerifySignature              (TPM_CC)(0x00000177)
#define TPM_CC_ECC_Parameters               (TPM_CC)(0x00000178)
#define TPM_CC_FirmwareRead                 (TPM_CC)(0x00000179)
#define TPM_CC_GetCapability                (TPM_CC)(0x0000017A)
#define TPM_CC_GetRandom                    (TPM_CC)(0x0000017B)
#define TPM_CC_GetTestResult                (TPM_CC)(0x0000017C)
#define TPM_CC_Hash                         (TPM_CC)(0x0000017D)
#define TPM_CC_PCR_Read                     (TPM_CC)(0x0000017E)
#define TPM_CC_PolicyPCR                    (TPM_CC)(0x0000017F)
#define TPM_CC_PolicyRestart                (TPM_CC)(0x00000180)
#define TPM_CC_ReadClock                    (TPM_CC)(0x00000181)
#define TPM_CC_PCR_Extend                   (TPM_CC)(0x00000182)
#define TPM_CC_PCR_SetAuthValue             (TPM_CC)(0x00000183)
#define TPM_CC_NV_Certify                   (TPM_CC)(0x00000184)
#define TPM_CC_EventSequenceComplete        (TPM_CC)(0x00000185)
#define TPM_CC_HashSequenceStart            (TPM_CC)(0x00000186)
#define TPM_CC_PolicyPhysicalPresence       (TPM_CC)(0x00000187)
#define TPM_CC_PolicyDuplicationSelect      (TPM_CC)(0x00000188)
#define TPM_CC_PolicyGetDigest              (TPM_CC)(0x00000189)
#define TPM_CC_TestParms                    (TPM_CC)(0x0000018A)
#define TPM_CC_Commit                       (TPM_CC)(0x0000018B)
#define TPM_CC_PolicyPassword               (TPM_CC)(0x0000018C)
#define TPM_CC_ZGen_2Phase                  (TPM_CC)(0x0000018D)
#define TPM_CC_EC_Ephemeral                 (TPM_CC)(0x0000018E)
#define TPM_CC_PolicyNvWritten              (TPM_CC)(0x0000018F)
#define TPM_CC_PolicyTemplate               (TPM_CC)(0x00000190)
#define TPM_CC_CreateLoaded                 (TPM_CC)(0x00000191)
#define TPM_CC_PolicyAuthorizeNV            (TPM_CC)(0x00000192)
#define TPM_CC_EncryptDecrypt2              (TPM_CC)(0x00000193)
#define TPM_CC_AC_GetCapability             (TPM_CC)(0x00000194)
#define TPM_CC_AC_Send                      (TPM_CC)(0x00000195)
#define TPM_CC_Policy_AC_SendSelect         (TPM_CC)(0x00000196)
#define TPM_CC_CertifyX509                  (TPM_CC)(0x00000197)
#define TPM_CC_ACT_SetTimeout               (TPM_CC)(0x00000198)
#define CC_VEND                             0x20000000
#define TPM_CC_Vendor_TCG_Test              (TPM_CC)(0x20000000)

#define NTC2_CC_PreConfig		    (TPM_CC)(0x20000211)
#define NTC2_CC_LockPreConfig               (TPM_CC)(0x20000212)
#define NTC2_CC_GetConfig                   (TPM_CC)(0x20000213)
/* Table 2:16 - Definition of TPM_RC Constants */
typedef UINT32             TPM_RC;
#define TYPE_OF_TPM_RC              UINT32
#define TPM_RC_SUCCESS              (TPM_RC)(0x000)
#define TPM_RC_BAD_TAG              (TPM_RC)(0x01E)
#define RC_VER1                     (TPM_RC)(0x100)
#define TPM_RC_INITIALIZE           (TPM_RC)(RC_VER1+0x000)
#define TPM_RC_FAILURE              (TPM_RC)(RC_VER1+0x001)
#define TPM_RC_SEQUENCE             (TPM_RC)(RC_VER1+0x003)
#define TPM_RC_PRIVATE              (TPM_RC)(RC_VER1+0x00B)
#define TPM_RC_HMAC                 (TPM_RC)(RC_VER1+0x019)
#define TPM_RC_DISABLED             (TPM_RC)(RC_VER1+0x020)
#define TPM_RC_EXCLUSIVE            (TPM_RC)(RC_VER1+0x021)
#define TPM_RC_AUTH_TYPE            (TPM_RC)(RC_VER1+0x024)
#define TPM_RC_AUTH_MISSING         (TPM_RC)(RC_VER1+0x025)
#define TPM_RC_POLICY               (TPM_RC)(RC_VER1+0x026)
#define TPM_RC_PCR                  (TPM_RC)(RC_VER1+0x027)
#define TPM_RC_PCR_CHANGED          (TPM_RC)(RC_VER1+0x028)
#define TPM_RC_UPGRADE              (TPM_RC)(RC_VER1+0x02D)
#define TPM_RC_TOO_MANY_CONTEXTS    (TPM_RC)(RC_VER1+0x02E)
#define TPM_RC_AUTH_UNAVAILABLE     (TPM_RC)(RC_VER1+0x02F)
#define TPM_RC_REBOOT               (TPM_RC)(RC_VER1+0x030)
#define TPM_RC_UNBALANCED           (TPM_RC)(RC_VER1+0x031)
#define TPM_RC_COMMAND_SIZE         (TPM_RC)(RC_VER1+0x042)
#define TPM_RC_COMMAND_CODE         (TPM_RC)(RC_VER1+0x043)
#define TPM_RC_AUTHSIZE             (TPM_RC)(RC_VER1+0x044)
#define TPM_RC_AUTH_CONTEXT         (TPM_RC)(RC_VER1+0x045)
#define TPM_RC_NV_RANGE             (TPM_RC)(RC_VER1+0x046)
#define TPM_RC_NV_SIZE              (TPM_RC)(RC_VER1+0x047)
#define TPM_RC_NV_LOCKED            (TPM_RC)(RC_VER1+0x048)
#define TPM_RC_NV_AUTHORIZATION     (TPM_RC)(RC_VER1+0x049)
#define TPM_RC_NV_UNINITIALIZED     (TPM_RC)(RC_VER1+0x04A)
#define TPM_RC_NV_SPACE             (TPM_RC)(RC_VER1+0x04B)
#define TPM_RC_NV_DEFINED           (TPM_RC)(RC_VER1+0x04C)
#define TPM_RC_BAD_CONTEXT          (TPM_RC)(RC_VER1+0x050)
#define TPM_RC_CPHASH               (TPM_RC)(RC_VER1+0x051)
#define TPM_RC_PARENT               (TPM_RC)(RC_VER1+0x052)
#define TPM_RC_NEEDS_TEST           (TPM_RC)(RC_VER1+0x053)
#define TPM_RC_NO_RESULT            (TPM_RC)(RC_VER1+0x054)
#define TPM_RC_SENSITIVE            (TPM_RC)(RC_VER1+0x055)
#define RC_MAX_FM0                  (TPM_RC)(RC_VER1+0x07F)
#define RC_FMT1                     (TPM_RC)(0x080)
#define TPM_RC_ASYMMETRIC           (TPM_RC)(RC_FMT1+0x001)
#define TPM_RCS_ASYMMETRIC          (TPM_RC)(RC_FMT1+0x001)
#define TPM_RC_ATTRIBUTES           (TPM_RC)(RC_FMT1+0x002)
#define TPM_RCS_ATTRIBUTES          (TPM_RC)(RC_FMT1+0x002)
#define TPM_RC_HASH                 (TPM_RC)(RC_FMT1+0x003)
#define TPM_RCS_HASH                (TPM_RC)(RC_FMT1+0x003)
#define TPM_RC_VALUE                (TPM_RC)(RC_FMT1+0x004)
#define TPM_RCS_VALUE               (TPM_RC)(RC_FMT1+0x004)
#define TPM_RC_HIERARCHY            (TPM_RC)(RC_FMT1+0x005)
#define TPM_RCS_HIERARCHY           (TPM_RC)(RC_FMT1+0x005)
#define TPM_RC_KEY_SIZE             (TPM_RC)(RC_FMT1+0x007)
#define TPM_RCS_KEY_SIZE            (TPM_RC)(RC_FMT1+0x007)
#define TPM_RC_MGF                  (TPM_RC)(RC_FMT1+0x008)
#define TPM_RCS_MGF                 (TPM_RC)(RC_FMT1+0x008)
#define TPM_RC_MODE                 (TPM_RC)(RC_FMT1+0x009)
#define TPM_RCS_MODE                (TPM_RC)(RC_FMT1+0x009)
#define TPM_RC_TYPE                 (TPM_RC)(RC_FMT1+0x00A)
#define TPM_RCS_TYPE                (TPM_RC)(RC_FMT1+0x00A)
#define TPM_RC_HANDLE               (TPM_RC)(RC_FMT1+0x00B)
#define TPM_RCS_HANDLE              (TPM_RC)(RC_FMT1+0x00B)
#define TPM_RC_KDF                  (TPM_RC)(RC_FMT1+0x00C)
#define TPM_RCS_KDF                 (TPM_RC)(RC_FMT1+0x00C)
#define TPM_RC_RANGE                (TPM_RC)(RC_FMT1+0x00D)
#define TPM_RCS_RANGE               (TPM_RC)(RC_FMT1+0x00D)
#define TPM_RC_AUTH_FAIL            (TPM_RC)(RC_FMT1+0x00E)
#define TPM_RCS_AUTH_FAIL           (TPM_RC)(RC_FMT1+0x00E)
#define TPM_RC_NONCE                (TPM_RC)(RC_FMT1+0x00F)
#define TPM_RCS_NONCE               (TPM_RC)(RC_FMT1+0x00F)
#define TPM_RC_PP                   (TPM_RC)(RC_FMT1+0x010)
#define TPM_RCS_PP                  (TPM_RC)(RC_FMT1+0x010)
#define TPM_RC_SCHEME               (TPM_RC)(RC_FMT1+0x012)
#define TPM_RCS_SCHEME              (TPM_RC)(RC_FMT1+0x012)
#define TPM_RC_SIZE                 (TPM_RC)(RC_FMT1+0x015)
#define TPM_RCS_SIZE                (TPM_RC)(RC_FMT1+0x015)
#define TPM_RC_SYMMETRIC            (TPM_RC)(RC_FMT1+0x016)
#define TPM_RCS_SYMMETRIC           (TPM_RC)(RC_FMT1+0x016)
#define TPM_RC_TAG                  (TPM_RC)(RC_FMT1+0x017)
#define TPM_RCS_TAG                 (TPM_RC)(RC_FMT1+0x017)
#define TPM_RC_SELECTOR             (TPM_RC)(RC_FMT1+0x018)
#define TPM_RCS_SELECTOR            (TPM_RC)(RC_FMT1+0x018)
#define TPM_RC_INSUFFICIENT         (TPM_RC)(RC_FMT1+0x01A)
#define TPM_RCS_INSUFFICIENT        (TPM_RC)(RC_FMT1+0x01A)
#define TPM_RC_SIGNATURE            (TPM_RC)(RC_FMT1+0x01B)
#define TPM_RCS_SIGNATURE           (TPM_RC)(RC_FMT1+0x01B)
#define TPM_RC_KEY                  (TPM_RC)(RC_FMT1+0x01C)
#define TPM_RCS_KEY                 (TPM_RC)(RC_FMT1+0x01C)
#define TPM_RC_POLICY_FAIL          (TPM_RC)(RC_FMT1+0x01D)
#define TPM_RCS_POLICY_FAIL         (TPM_RC)(RC_FMT1+0x01D)
#define TPM_RC_INTEGRITY            (TPM_RC)(RC_FMT1+0x01F)
#define TPM_RCS_INTEGRITY           (TPM_RC)(RC_FMT1+0x01F)
#define TPM_RC_TICKET               (TPM_RC)(RC_FMT1+0x020)
#define TPM_RCS_TICKET              (TPM_RC)(RC_FMT1+0x020)
#define TPM_RC_RESERVED_BITS        (TPM_RC)(RC_FMT1+0x021)
#define TPM_RCS_RESERVED_BITS       (TPM_RC)(RC_FMT1+0x021)
#define TPM_RC_BAD_AUTH             (TPM_RC)(RC_FMT1+0x022)
#define TPM_RCS_BAD_AUTH            (TPM_RC)(RC_FMT1+0x022)
#define TPM_RC_EXPIRED              (TPM_RC)(RC_FMT1+0x023)
#define TPM_RCS_EXPIRED             (TPM_RC)(RC_FMT1+0x023)
#define TPM_RC_POLICY_CC            (TPM_RC)(RC_FMT1+0x024)
#define TPM_RCS_POLICY_CC           (TPM_RC)(RC_FMT1+0x024)
#define TPM_RC_BINDING              (TPM_RC)(RC_FMT1+0x025)
#define TPM_RCS_BINDING             (TPM_RC)(RC_FMT1+0x025)
#define TPM_RC_CURVE                (TPM_RC)(RC_FMT1+0x026)
#define TPM_RCS_CURVE               (TPM_RC)(RC_FMT1+0x026)
#define TPM_RC_ECC_POINT            (TPM_RC)(RC_FMT1+0x027)
#define TPM_RCS_ECC_POINT           (TPM_RC)(RC_FMT1+0x027)
#define RC_WARN                     (TPM_RC)(0x900)
#define TPM_RC_CONTEXT_GAP          (TPM_RC)(RC_WARN+0x001)
#define TPM_RC_OBJECT_MEMORY        (TPM_RC)(RC_WARN+0x002)
#define TPM_RC_SESSION_MEMORY       (TPM_RC)(RC_WARN+0x003)
#define TPM_RC_MEMORY               (TPM_RC)(RC_WARN+0x004)
#define TPM_RC_SESSION_HANDLES      (TPM_RC)(RC_WARN+0x005)
#define TPM_RC_OBJECT_HANDLES       (TPM_RC)(RC_WARN+0x006)
#define TPM_RC_LOCALITY             (TPM_RC)(RC_WARN+0x007)
#define TPM_RC_YIELDED              (TPM_RC)(RC_WARN+0x008)
#define TPM_RC_CANCELED             (TPM_RC)(RC_WARN+0x009)
#define TPM_RC_TESTING              (TPM_RC)(RC_WARN+0x00A)
#define TPM_RC_REFERENCE_H0         (TPM_RC)(RC_WARN+0x010)
#define TPM_RC_REFERENCE_H1         (TPM_RC)(RC_WARN+0x011)
#define TPM_RC_REFERENCE_H2         (TPM_RC)(RC_WARN+0x012)
#define TPM_RC_REFERENCE_H3         (TPM_RC)(RC_WARN+0x013)
#define TPM_RC_REFERENCE_H4         (TPM_RC)(RC_WARN+0x014)
#define TPM_RC_REFERENCE_H5         (TPM_RC)(RC_WARN+0x015)
#define TPM_RC_REFERENCE_H6         (TPM_RC)(RC_WARN+0x016)
#define TPM_RC_REFERENCE_S0         (TPM_RC)(RC_WARN+0x018)
#define TPM_RC_REFERENCE_S1         (TPM_RC)(RC_WARN+0x019)
#define TPM_RC_REFERENCE_S2         (TPM_RC)(RC_WARN+0x01A)
#define TPM_RC_REFERENCE_S3         (TPM_RC)(RC_WARN+0x01B)
#define TPM_RC_REFERENCE_S4         (TPM_RC)(RC_WARN+0x01C)
#define TPM_RC_REFERENCE_S5         (TPM_RC)(RC_WARN+0x01D)
#define TPM_RC_REFERENCE_S6         (TPM_RC)(RC_WARN+0x01E)
#define TPM_RC_NV_RATE              (TPM_RC)(RC_WARN+0x020)
#define TPM_RC_LOCKOUT              (TPM_RC)(RC_WARN+0x021)
#define TPM_RC_RETRY                (TPM_RC)(RC_WARN+0x022)
#define TPM_RC_NV_UNAVAILABLE       (TPM_RC)(RC_WARN+0x023)
#define TPM_RC_NOT_USED             (TPM_RC)(RC_WARN+0x7F)
#define TPM_RC_H                    (TPM_RC)(0x000)
#define TPM_RC_P                    (TPM_RC)(0x040)
#define TPM_RC_S                    (TPM_RC)(0x800)
#define TPM_RC_1                    (TPM_RC)(0x100)
#define TPM_RC_2                    (TPM_RC)(0x200)
#define TPM_RC_3                    (TPM_RC)(0x300)
#define TPM_RC_4                    (TPM_RC)(0x400)
#define TPM_RC_5                    (TPM_RC)(0x500)
#define TPM_RC_6                    (TPM_RC)(0x600)
#define TPM_RC_7                    (TPM_RC)(0x700)
#define TPM_RC_8                    (TPM_RC)(0x800)
#define TPM_RC_9                    (TPM_RC)(0x900)
#define TPM_RC_A                    (TPM_RC)(0xA00)
#define TPM_RC_B                    (TPM_RC)(0xB00)
#define TPM_RC_C                    (TPM_RC)(0xC00)
#define TPM_RC_D                    (TPM_RC)(0xD00)
#define TPM_RC_E                    (TPM_RC)(0xE00)
#define TPM_RC_F                    (TPM_RC)(0xF00)
#define TPM_RC_N_MASK               (TPM_RC)(0xF00)

/* Table 2:17 - Definition of TPM_CLOCK_ADJUST Constants */
typedef BYTE               TPM_CLOCK_ADJUST;
#define TYPE_OF_TPM_CLOCK_ADJUST    BYTE
#define TPM_CLOCK_COARSE_SLOWER    (TPM_CLOCK_ADJUST)(-3)
#define TPM_CLOCK_MEDIUM_SLOWER    (TPM_CLOCK_ADJUST)(-2)
#define TPM_CLOCK_FINE_SLOWER      (TPM_CLOCK_ADJUST)(-1)
#define TPM_CLOCK_NO_CHANGE        (TPM_CLOCK_ADJUST)(0)
#define TPM_CLOCK_FINE_FASTER      (TPM_CLOCK_ADJUST)(1)
#define TPM_CLOCK_MEDIUM_FASTER    (TPM_CLOCK_ADJUST)(2)
#define TPM_CLOCK_COARSE_FASTER    (TPM_CLOCK_ADJUST)(3)
/* Table 2:18 - Definition of TPM_EO Constants */
typedef  UINT16             TPM_EO;
#define TYPE_OF_TPM_EO      UINT16
#define TPM_EO_EQ             (TPM_EO)(0x0000)
#define TPM_EO_NEQ            (TPM_EO)(0x0001)
#define TPM_EO_SIGNED_GT      (TPM_EO)(0x0002)
#define TPM_EO_UNSIGNED_GT    (TPM_EO)(0x0003)
#define TPM_EO_SIGNED_LT      (TPM_EO)(0x0004)
#define TPM_EO_UNSIGNED_LT    (TPM_EO)(0x0005)
#define TPM_EO_SIGNED_GE      (TPM_EO)(0x0006)
#define TPM_EO_UNSIGNED_GE    (TPM_EO)(0x0007)
#define TPM_EO_SIGNED_LE      (TPM_EO)(0x0008)
#define TPM_EO_UNSIGNED_LE    (TPM_EO)(0x0009)
#define TPM_EO_BITSET         (TPM_EO)(0x000A)
#define TPM_EO_BITCLEAR       (TPM_EO)(0x000B)
/* Table 2:19 - Definition of TPM_ST Constants */
typedef  UINT16             TPM_ST;
#define TYPE_OF_TPM_ST                  UINT16
#define TPM_ST_RSP_COMMAND             (TPM_ST)(0x00C4)
#define TPM_ST_NULL                    (TPM_ST)(0x8000)
#define TPM_ST_NO_SESSIONS             (TPM_ST)(0x8001)
#define TPM_ST_SESSIONS                (TPM_ST)(0x8002)
#define TPM_ST_ATTEST_NV               (TPM_ST)(0x8014)
#define TPM_ST_ATTEST_COMMAND_AUDIT    (TPM_ST)(0x8015)
#define TPM_ST_ATTEST_SESSION_AUDIT    (TPM_ST)(0x8016)
#define TPM_ST_ATTEST_CERTIFY          (TPM_ST)(0x8017)
#define TPM_ST_ATTEST_QUOTE            (TPM_ST)(0x8018)
#define TPM_ST_ATTEST_TIME             (TPM_ST)(0x8019)
#define TPM_ST_ATTEST_CREATION         (TPM_ST)(0x801A)
#define TPM_ST_ATTEST_NV_DIGEST	       (TPM_ST)(0x801C)
#define TPM_ST_CREATION                (TPM_ST)(0x8021)
#define TPM_ST_VERIFIED                (TPM_ST)(0x8022)
#define TPM_ST_AUTH_SECRET             (TPM_ST)(0x8023)
#define TPM_ST_HASHCHECK               (TPM_ST)(0x8024)
#define TPM_ST_AUTH_SIGNED             (TPM_ST)(0x8025)
#define TPM_ST_FU_MANIFEST             (TPM_ST)(0x8029)

/* Table 2:20 - Definition of TPM_SU Constants */
typedef UINT16             TPM_SU;
#define TYPE_OF_TPM_SU      UINT16
#define TPM_SU_CLEAR    (TPM_SU)(0x0000)
#define TPM_SU_STATE    (TPM_SU)(0x0001)
/* Table 2:21 - Definition of TPM_SE Constants */
typedef BYTE              TPM_SE;
#define TYPE_OF_TPM_SE      BYTE
#define TPM_SE_HMAC      (TPM_SE)(0x00)
#define TPM_SE_POLICY    (TPM_SE)(0x01)
#define TPM_SE_TRIAL     (TPM_SE)(0x03)

/* Table 2:22 - Definition of TPM_CAP Constants */
typedef UINT32             TPM_CAP;
#define TYPE_OF_TPM_CAP             UINT32
#define TPM_CAP_FIRST              (TPM_CAP)(0x00000000)
#define TPM_CAP_ALGS               (TPM_CAP)(0x00000000)
#define TPM_CAP_HANDLES            (TPM_CAP)(0x00000001)
#define TPM_CAP_COMMANDS           (TPM_CAP)(0x00000002)
#define TPM_CAP_PP_COMMANDS        (TPM_CAP)(0x00000003)
#define TPM_CAP_AUDIT_COMMANDS     (TPM_CAP)(0x00000004)
#define TPM_CAP_PCRS               (TPM_CAP)(0x00000005)
#define TPM_CAP_TPM_PROPERTIES     (TPM_CAP)(0x00000006)
#define TPM_CAP_PCR_PROPERTIES     (TPM_CAP)(0x00000007)
#define TPM_CAP_ECC_CURVES         (TPM_CAP)(0x00000008)
#define TPM_CAP_AUTH_POLICIES      (TPM_CAP)(0x00000009)
#define TPM_CAP_ACT 		   (TPM_CAP)(0x0000000a)
#define TPM_CAP_LAST               (TPM_CAP)(0x0000000a)
#define TPM_CAP_VENDOR_PROPERTY    (TPM_CAP)(0x00000100)
/* Table 2:23 - Definition of TPM_PT Constants */
typedef  UINT32             TPM_PT;
#define TYPE_OF_TPM_PT              UINT32
#define TPM_PT_NONE                   (TPM_PT)(0x00000000)
#define PT_GROUP                      (TPM_PT)(0x00000100)
#define PT_FIXED                      (TPM_PT)(PT_GROUP*1)
#define TPM_PT_FAMILY_INDICATOR       (TPM_PT)(PT_FIXED+0)
#define TPM_PT_LEVEL                  (TPM_PT)(PT_FIXED+1)
#define TPM_PT_REVISION               (TPM_PT)(PT_FIXED+2)
#define TPM_PT_DAY_OF_YEAR            (TPM_PT)(PT_FIXED+3)
#define TPM_PT_YEAR                   (TPM_PT)(PT_FIXED+4)
#define TPM_PT_MANUFACTURER           (TPM_PT)(PT_FIXED+5)
#define TPM_PT_VENDOR_STRING_1        (TPM_PT)(PT_FIXED+6)
#define TPM_PT_VENDOR_STRING_2        (TPM_PT)(PT_FIXED+7)
#define TPM_PT_VENDOR_STRING_3        (TPM_PT)(PT_FIXED+8)
#define TPM_PT_VENDOR_STRING_4        (TPM_PT)(PT_FIXED+9)
#define TPM_PT_VENDOR_TPM_TYPE        (TPM_PT)(PT_FIXED+10)
#define TPM_PT_FIRMWARE_VERSION_1     (TPM_PT)(PT_FIXED+11)
#define TPM_PT_FIRMWARE_VERSION_2     (TPM_PT)(PT_FIXED+12)
#define TPM_PT_INPUT_BUFFER           (TPM_PT)(PT_FIXED+13)
#define TPM_PT_HR_TRANSIENT_MIN       (TPM_PT)(PT_FIXED+14)
#define TPM_PT_HR_PERSISTENT_MIN      (TPM_PT)(PT_FIXED+15)
#define TPM_PT_HR_LOADED_MIN          (TPM_PT)(PT_FIXED+16)
#define TPM_PT_ACTIVE_SESSIONS_MAX    (TPM_PT)(PT_FIXED+17)
#define TPM_PT_PCR_COUNT              (TPM_PT)(PT_FIXED+18)
#define TPM_PT_PCR_SELECT_MIN         (TPM_PT)(PT_FIXED+19)
#define TPM_PT_CONTEXT_GAP_MAX        (TPM_PT)(PT_FIXED+20)
#define TPM_PT_NV_COUNTERS_MAX        (TPM_PT)(PT_FIXED+22)
#define TPM_PT_NV_INDEX_MAX           (TPM_PT)(PT_FIXED+23)
#define TPM_PT_MEMORY                 (TPM_PT)(PT_FIXED+24)
#define TPM_PT_CLOCK_UPDATE           (TPM_PT)(PT_FIXED+25)
#define TPM_PT_CONTEXT_HASH           (TPM_PT)(PT_FIXED+26)
#define TPM_PT_CONTEXT_SYM            (TPM_PT)(PT_FIXED+27)
#define TPM_PT_CONTEXT_SYM_SIZE       (TPM_PT)(PT_FIXED+28)
#define TPM_PT_ORDERLY_COUNT          (TPM_PT)(PT_FIXED+29)
#define TPM_PT_MAX_COMMAND_SIZE       (TPM_PT)(PT_FIXED+30)
#define TPM_PT_MAX_RESPONSE_SIZE      (TPM_PT)(PT_FIXED+31)
#define TPM_PT_MAX_DIGEST             (TPM_PT)(PT_FIXED+32)
#define TPM_PT_MAX_OBJECT_CONTEXT     (TPM_PT)(PT_FIXED+33)
#define TPM_PT_MAX_SESSION_CONTEXT    (TPM_PT)(PT_FIXED+34)
#define TPM_PT_PS_FAMILY_INDICATOR    (TPM_PT)(PT_FIXED+35)
#define TPM_PT_PS_LEVEL               (TPM_PT)(PT_FIXED+36)
#define TPM_PT_PS_REVISION            (TPM_PT)(PT_FIXED+37)
#define TPM_PT_PS_DAY_OF_YEAR         (TPM_PT)(PT_FIXED+38)
#define TPM_PT_PS_YEAR                (TPM_PT)(PT_FIXED+39)
#define TPM_PT_SPLIT_MAX              (TPM_PT)(PT_FIXED+40)
#define TPM_PT_TOTAL_COMMANDS         (TPM_PT)(PT_FIXED+41)
#define TPM_PT_LIBRARY_COMMANDS       (TPM_PT)(PT_FIXED+42)
#define TPM_PT_VENDOR_COMMANDS        (TPM_PT)(PT_FIXED+43)
#define TPM_PT_NV_BUFFER_MAX          (TPM_PT)(PT_FIXED+44)
#define TPM_PT_MODES                  (TPM_PT)(PT_FIXED+45)
#define TPM_PT_MAX_CAP_BUFFER         (TPM_PT)(PT_FIXED+46)
#define PT_VAR                        (TPM_PT)(PT_GROUP*2)
#define TPM_PT_PERMANENT              (TPM_PT)(PT_VAR+0)
#define TPM_PT_STARTUP_CLEAR          (TPM_PT)(PT_VAR+1)
#define TPM_PT_HR_NV_INDEX            (TPM_PT)(PT_VAR+2)
#define TPM_PT_HR_LOADED              (TPM_PT)(PT_VAR+3)
#define TPM_PT_HR_LOADED_AVAIL        (TPM_PT)(PT_VAR+4)
#define TPM_PT_HR_ACTIVE              (TPM_PT)(PT_VAR+5)
#define TPM_PT_HR_ACTIVE_AVAIL        (TPM_PT)(PT_VAR+6)
#define TPM_PT_HR_TRANSIENT_AVAIL     (TPM_PT)(PT_VAR+7)
#define TPM_PT_HR_PERSISTENT          (TPM_PT)(PT_VAR+8)
#define TPM_PT_HR_PERSISTENT_AVAIL    (TPM_PT)(PT_VAR+9)
#define TPM_PT_NV_COUNTERS            (TPM_PT)(PT_VAR+10)
#define TPM_PT_NV_COUNTERS_AVAIL      (TPM_PT)(PT_VAR+11)
#define TPM_PT_ALGORITHM_SET          (TPM_PT)(PT_VAR+12)
#define TPM_PT_LOADED_CURVES          (TPM_PT)(PT_VAR+13)
#define TPM_PT_LOCKOUT_COUNTER        (TPM_PT)(PT_VAR+14)
#define TPM_PT_MAX_AUTH_FAIL          (TPM_PT)(PT_VAR+15)
#define TPM_PT_LOCKOUT_INTERVAL       (TPM_PT)(PT_VAR+16)
#define TPM_PT_LOCKOUT_RECOVERY       (TPM_PT)(PT_VAR+17)
#define TPM_PT_NV_WRITE_RECOVERY      (TPM_PT)(PT_VAR+18)
#define TPM_PT_AUDIT_COUNTER_0        (TPM_PT)(PT_VAR+19)
#define TPM_PT_AUDIT_COUNTER_1        (TPM_PT)(PT_VAR+20)

/* Table 2:24 - Definition of TPM_PT_PCR Constants */
typedef UINT32             TPM_PT_PCR;
#define TYPE_OF_TPM_PT_PCR          UINT32
#define TPM_PT_PCR_FIRST           (TPM_PT_PCR)(0x00000000)
#define TPM_PT_PCR_SAVE            (TPM_PT_PCR)(0x00000000)
#define TPM_PT_PCR_EXTEND_L0       (TPM_PT_PCR)(0x00000001)
#define TPM_PT_PCR_RESET_L0        (TPM_PT_PCR)(0x00000002)
#define TPM_PT_PCR_EXTEND_L1       (TPM_PT_PCR)(0x00000003)
#define TPM_PT_PCR_RESET_L1        (TPM_PT_PCR)(0x00000004)
#define TPM_PT_PCR_EXTEND_L2       (TPM_PT_PCR)(0x00000005)
#define TPM_PT_PCR_RESET_L2        (TPM_PT_PCR)(0x00000006)
#define TPM_PT_PCR_EXTEND_L3       (TPM_PT_PCR)(0x00000007)
#define TPM_PT_PCR_RESET_L3        (TPM_PT_PCR)(0x00000008)
#define TPM_PT_PCR_EXTEND_L4       (TPM_PT_PCR)(0x00000009)
#define TPM_PT_PCR_RESET_L4        (TPM_PT_PCR)(0x0000000A)
#define TPM_PT_PCR_NO_INCREMENT    (TPM_PT_PCR)(0x00000011)
#define TPM_PT_PCR_DRTM_RESET      (TPM_PT_PCR)(0x00000012)
#define TPM_PT_PCR_POLICY          (TPM_PT_PCR)(0x00000013)
#define TPM_PT_PCR_AUTH            (TPM_PT_PCR)(0x00000014)
#define TPM_PT_PCR_LAST            (TPM_PT_PCR)(0x00000014)

/* Table 2:25 - Definition of TPM_PS Constants  */
typedef UINT32             TPM_PS;
#define TYPE_OF_TPM_PS          UINT32
#define TPM_PS_MAIN              (TPM_PS)(0x00000000)
#define TPM_PS_PC                (TPM_PS)(0x00000001)
#define TPM_PS_PDA               (TPM_PS)(0x00000002)
#define TPM_PS_CELL_PHONE        (TPM_PS)(0x00000003)
#define TPM_PS_SERVER            (TPM_PS)(0x00000004)
#define TPM_PS_PERIPHERAL        (TPM_PS)(0x00000005)
#define TPM_PS_TSS               (TPM_PS)(0x00000006)
#define TPM_PS_STORAGE           (TPM_PS)(0x00000007)
#define TPM_PS_AUTHENTICATION    (TPM_PS)(0x00000008)
#define TPM_PS_EMBEDDED          (TPM_PS)(0x00000009)
#define TPM_PS_HARDCOPY          (TPM_PS)(0x0000000A)
#define TPM_PS_INFRASTRUCTURE    (TPM_PS)(0x0000000B)
#define TPM_PS_VIRTUALIZATION    (TPM_PS)(0x0000000C)
#define TPM_PS_TNC               (TPM_PS)(0x0000000D)
#define TPM_PS_MULTI_TENANT      (TPM_PS)(0x0000000E)
#define TPM_PS_TC                (TPM_PS)(0x0000000F)
/* Table 2:26 - Definition of Types for Handles */
typedef UINT32 TPM_HANDLE;

/* Table 2:27 - Definition of TPM_HT Constants  */
typedef BYTE              TPM_HT;
#define TYPE_OF_TPM_HT          BYTE
#define TPM_HT_PCR               (TPM_HT)(0x00)
#define TPM_HT_NV_INDEX          (TPM_HT)(0x01)
#define TPM_HT_HMAC_SESSION      (TPM_HT)(0x02)
#define TPM_HT_LOADED_SESSION    (TPM_HT)(0x02)
#define TPM_HT_POLICY_SESSION    (TPM_HT)(0x03)
#define TPM_HT_SAVED_SESSION     (TPM_HT)(0x03)
#define TPM_HT_PERMANENT         (TPM_HT)(0x40)
#define TPM_HT_TRANSIENT         (TPM_HT)(0x80)
#define TPM_HT_PERSISTENT        (TPM_HT)(0x81)
#define TPM_HT_AC                (TPM_HT)(0x90)
/* Table 2:28 - Definition of TPM_RH Constants  */
typedef  TPM_HANDLE         TPM_RH;
#define  TPM_RH_FIRST          (TPM_RH)(0x40000000)
#define  TPM_RH_SRK            (TPM_RH)(0x40000000)
#define  TPM_RH_OWNER          (TPM_RH)(0x40000001)
#define  TPM_RH_REVOKE         (TPM_RH)(0x40000002)
#define  TPM_RH_TRANSPORT      (TPM_RH)(0x40000003)
#define  TPM_RH_OPERATOR       (TPM_RH)(0x40000004)
#define  TPM_RH_ADMIN          (TPM_RH)(0x40000005)
#define  TPM_RH_EK             (TPM_RH)(0x40000006)
#define  TPM_RH_NULL           (TPM_RH)(0x40000007)
#define  TPM_RH_UNASSIGNED     (TPM_RH)(0x40000008)
#define  TPM_RS_PW             (TPM_RH)(0x40000009)
#define  TPM_RH_LOCKOUT        (TPM_RH)(0x4000000A)
#define  TPM_RH_ENDORSEMENT    (TPM_RH)(0x4000000B)
#define  TPM_RH_PLATFORM       (TPM_RH)(0x4000000C)
#define  TPM_RH_PLATFORM_NV    (TPM_RH)(0x4000000D)
#define  TPM_RH_AUTH_00        (TPM_RH)(0x40000010)
#define  TPM_RH_AUTH_FF        (TPM_RH)(0x4000010F)
#define  TPM_RH_ACT_0          (TPM_RH)(0x40000110)
#define  TPM_RH_ACT_F          (TPM_RH)(0x4000011F)
#define  TPM_RH_LAST           (TPM_RH)(0x4000011F)

/* Table 2:29 - Definition of TPM_HC Constants  */
typedef  TPM_HANDLE         TPM_HC;
#define  HR_HANDLE_MASK          (TPM_HC)(0x00FFFFFF)
#define  HR_RANGE_MASK           (TPM_HC)(0xFF000000)
#define  HR_SHIFT                (TPM_HC)(24)
#define  HR_PCR                  (TPM_HC)((TPM_HT_PCR<<HR_SHIFT))
#define  HR_HMAC_SESSION         (TPM_HC)((TPM_HT_HMAC_SESSION<<HR_SHIFT))
#define  HR_POLICY_SESSION       (TPM_HC)((TPM_HT_POLICY_SESSION<<HR_SHIFT))

#define  HR_TRANSIENT		 (TPM_HC)((((UINT32)TPM_HT_TRANSIENT) << HR_SHIFT))
#define  HR_PERSISTENT           (TPM_HC)((((UINT32)TPM_HT_PERSISTENT) << HR_SHIFT))

#if 0
#define  HR_TRANSIENT            (TPM_HC)((TPM_HT_TRANSIENT<<HR_SHIFT))
#define  HR_PERSISTENT           (TPM_HC)((TPM_HT_PERSISTENT<<HR_SHIFT))
#endif

#define  HR_NV_INDEX             (TPM_HC)((TPM_HT_NV_INDEX<<HR_SHIFT))
#define  HR_PERMANENT            (TPM_HC)((TPM_HT_PERMANENT<<HR_SHIFT))
#define  PCR_FIRST               (TPM_HC)((HR_PCR+0))
#define  PCR_LAST                (TPM_HC)((PCR_FIRST+IMPLEMENTATION_PCR-1))
#define  HMAC_SESSION_FIRST      (TPM_HC)((HR_HMAC_SESSION+0))
#define  HMAC_SESSION_LAST       (TPM_HC)((HMAC_SESSION_FIRST + MAX_ACTIVE_SESSIONS-1))
#define  LOADED_SESSION_FIRST    (TPM_HC)(HMAC_SESSION_FIRST)
#define  LOADED_SESSION_LAST     (TPM_HC)(HMAC_SESSION_LAST)
#define  POLICY_SESSION_FIRST    (TPM_HC)((HR_POLICY_SESSION+0))
#define  POLICY_SESSION_LAST	 (TPM_HC)((POLICY_SESSION_FIRST + MAX_ACTIVE_SESSIONS-1))
#define  TRANSIENT_FIRST         (TPM_HC)((HR_TRANSIENT+0))
#define  ACTIVE_SESSION_FIRST    (TPM_HC)(POLICY_SESSION_FIRST)
#define  ACTIVE_SESSION_LAST     (TPM_HC)(POLICY_SESSION_LAST)
#define  TRANSIENT_LAST          (TPM_HC)((TRANSIENT_FIRST+MAX_LOADED_OBJECTS-1))
#define  PERSISTENT_FIRST        (TPM_HC)((HR_PERSISTENT+0))
#define  PERSISTENT_LAST         (TPM_HC)((PERSISTENT_FIRST+0x00FFFFFF))
#define  PLATFORM_PERSISTENT     (TPM_HC)((PERSISTENT_FIRST+0x00800000))
#define  NV_INDEX_FIRST          (TPM_HC)((HR_NV_INDEX+0))
#define  NV_INDEX_LAST           (TPM_HC)((NV_INDEX_FIRST+0x00FFFFFF))
#define  PERMANENT_FIRST         (TPM_HC)(TPM_RH_FIRST)
#define  PERMANENT_LAST          (TPM_HC)(TPM_RH_LAST)
#define  HR_NV_AC                (TPM_HC)(((TPM_HT_NV_INDEX<<HR_SHIFT)+0xD00000))
#define  NV_AC_FIRST             (TPM_HC)((HR_NV_AC+0))
#define  NV_AC_LAST              (TPM_HC)((HR_NV_AC+0x0000FFFF))
#define  HR_AC                   (TPM_HC)((TPM_HT_AC<<HR_SHIFT))
#define  AC_FIRST                (TPM_HC)((HR_AC+0))
#define  AC_LAST                 (TPM_HC)((HR_AC+0x0000FFFF))

/* Table 2:30 - Definition of TPMA_ALGORITHM Bits */
typedef struct TPMA_ALGORITHM{
    unsigned    asymmetric           : 1 ;
    unsigned    symmetric            : 1 ;
    unsigned    hash                 : 1 ;
    unsigned    object               : 1 ;
    unsigned    Reserved_bits_at_4   : 4 ;
    unsigned    signing              : 1 ;
    unsigned    encrypting           : 1 ;
    unsigned    method               : 1 ;
    unsigned    Reserved_bits_at_11  : 21;
} __attribute__((packed)) TPMA_ALGORITHM;

#define TYPE_OF_TPMA_ALGORITHM  UINT32
#define TPMA_ALGORITHM_TO_UINT32(a)  (*((UINT32 *)&(a)))
#define UINT32_TO_TPMA_ALGORITHM(a)  (*((TPMA_ALGORITHM *)&(a)))

/* The aggregation macros for machines that do not allow unaligned access or for little-endian
   machines. Aggregate bytes into an UINT */
#define BYTE_ARRAY_TO_UINT8(b)  (uint8_t)((b)[0])
#define BYTE_ARRAY_TO_UINT16(b) ByteArrayToUint16((BYTE *)(b))
#define BYTE_ARRAY_TO_UINT32(b) ByteArrayToUint32((BYTE *)(b))
#define BYTE_ARRAY_TO_UINT64(b) ByteArrayToUint64((BYTE *)(b))
#define UINT8_TO_BYTE_ARRAY(i, b) ((b)[0] = (uint8_t)(i))
#define UINT16_TO_BYTE_ARRAY(i, b)  UINT16ToArray((i), (BYTE *)(b))
#define UINT32_TO_BYTE_ARRAY(i, b)  UINT32ToArray((i), (BYTE *)(b))
#define UINT64_TO_BYTE_ARRAY(i, b)  UINT64ToArray((i), (BYTE *)(b))

#define TPMA_ALGORITHM_TO_BYTE_ARRAY(i, a)				\
	            UINT32_TO_BYTE_ARRAY((TPMA_ALGORITHM_TO_UINT32(i)), (a))

#define BYTE_ARRAY_TO_TPMA_ALGORITHM(i, a)				\
    {UINT32 x = BYTE_ARRAY_TO_UINT32(a);				\
	i = UINT32_TO_TPMA_ALGORITHM(x);				\
    }

#define TYPE_OF_TPMA_SESSION    BYTE
#define TPMA_SESSION_TO_BYTE(a)     (*((BYTE *)&(a)))
#define BYTE_TO_TPMA_SESSION(a)     (*((TPMA_SESSION *)&(a)))
#define TPMA_SESSION_TO_BYTE_ARRAY(i, a)                                           \
            BYTE_TO_BYTE_ARRAY((TPMA_SESSION_TO_BYTE(i)), (a))
#define BYTE_ARRAY_TO_TPMA_SESSION(i, a)                                           \
            { BYTE x = BYTE_ARRAY_TO_BYTE(a); i = BYTE_TO_TPMA_SESSION(x); }
typedef struct TPMA_SESSION {                       // Table 2:32
    unsigned    continueSession      : 1;
    unsigned    auditExclusive       : 1;
    unsigned    auditReset           : 1;
    unsigned    Reserved_bits_at_3   : 2;
    unsigned    decrypt              : 1;
    unsigned    encrypt              : 1;
    unsigned    audit                : 1;
} __attribute__((packed)) TPMA_SESSION;
// This is the initializer for a TPMA_SESSION structure
#define TPMA_SESSION_INITIALIZER(					\
				 continuesession, auditexclusive,  auditreset,      bits_at_3, \
				 decrypt,         encrypt,         audit) \
    {continuesession, auditexclusive,  auditreset,      bits_at_3,	\
	    decrypt,         encrypt,         audit}
#define TYPE_OF_TPMA_CC     UINT32
#define TPMA_CC_TO_UINT32(a)     (*((UINT32 *)&(a)))
#define UINT32_TO_TPMA_CC(a)     (*((TPMA_CC *)&(a)))
#define TPMA_CC_TO_BYTE_ARRAY(i, a)                                                \
            UINT32_TO_BYTE_ARRAY((TPMA_CC_TO_UINT32(i)), (a))
#define BYTE_ARRAY_TO_TPMA_CC(i, a)                                                \
            { UINT32 x = BYTE_ARRAY_TO_UINT32(a); i = UINT32_TO_TPMA_CC(x); }
typedef struct TPMA_CC {                            // Table 2:37
    unsigned    commandIndex         : 16;
    unsigned    Reserved_bits_at_16  : 6;
    unsigned    nv                   : 1;
    unsigned    extensive            : 1;
    unsigned    flushed              : 1;
    unsigned    cHandles             : 3;
    unsigned    rHandle              : 1;
    unsigned    V                    : 1;
    unsigned    Reserved_bits_at_30  : 2;
} __attribute__((packed)) TPMA_CC;
// This is the initializer for a TPMA_CC structure
#define TPMA_CC_INITIALIZER(						\
			    commandindex, bits_at_16,   nv,           extensive,    flushed, \
			    chandles,     rhandle,      v,            bits_at_30) \
    {commandindex, bits_at_16,   nv,           extensive,    flushed,	\
	    chandles,     rhandle,      v,            bits_at_30}

/* 5.23	TpmError.h */

#define     FATAL_ERROR_ALLOCATION              (1)
#define     FATAL_ERROR_DIVIDE_ZERO             (2)
#define     FATAL_ERROR_INTERNAL                (3)
#define     FATAL_ERROR_PARAMETER               (4)
#define     FATAL_ERROR_ENTROPY                 (5)
#define     FATAL_ERROR_SELF_TEST               (6)
#define     FATAL_ERROR_CRYPTO                  (7)
#define     FATAL_ERROR_NV_UNRECOVERABLE        (8)
#define     FATAL_ERROR_REMANUFACTURED          (9) // indicates that the TPM has
                                                    // been re-manufactured after an
                                                    // unrecoverable NV error
#define     FATAL_ERROR_DRBG                    (10)
#define     FATAL_ERROR_MOVE_SIZE               (11)
#define     FATAL_ERROR_COUNTER_OVERFLOW        (12)
#define     FATAL_ERROR_SUBTRACT                (13)
#define     FATAL_ERROR_FORCED                  (666)

#endif
