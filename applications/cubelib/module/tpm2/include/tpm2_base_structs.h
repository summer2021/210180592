/********************************************************************************/
/********************************************************************************/
#ifndef TPM2_BASE_STRUCTURE_H
#define TPM2_BASE_STRUCTURE_H

/* TPM2B Types */
typedef struct tpm2b
{
    UINT16 size;
    BYTE *buffer;
} __attribute__((packed)) TPM2B, *P2B;

typedef struct tpm2b_digest
{
    TPM2B b;
} __attribute__((packed)) TPM2B_DIGEST;

typedef struct tpm2b_data
{
    TPM2B b;
} __attribute__((packed)) TPM2B_DATA;

typedef struct tpm2b_name
{
    TPM2B b;
} __attribute__((packed)) RECORD(TPM2_KEY, TPM2B_NAME);
typedef RECORD(TPM2_KEY, TPM2B_NAME) TPM2B_NAME;

typedef struct tpm2b_ecc_parameter
{
    TPM2B b;
} __attribute__((packed)) RECORD(TPM2_KEY, TPM2B_ECC_PARAMETER);
typedef RECORD(TPM2_KEY, TPM2B_ECC_PARAMETER) TPM2B_ECC_PARAMETER;

typedef struct tpm2b_label
{
    TPM2B b;
} __attribute__((packed)) RECORD(TPM2_KEY, TPM2B_LABEL);
typedef RECORD(TPM2_KEY, TPM2B_LABEL) TPM2B_LABEL;

typedef struct tpm2b_private
{
    TPM2B b;
} __attribute__((packed)) TPM2B_PRIVATE;
typedef struct tpm2b_sensitive_data
{
        TPM2B        b;
} __attribute__((packed)) TPM2B_SENSITIVE_DATA;

typedef TPM2B_DIGEST TPM2B_NONCE;
typedef TPM2B_DIGEST TPM2B_AUTH;
typedef TPM2B_DIGEST TPM2B_OPERAND;




/* Table 2:39 - Definition of TPMI_YES_NO Type  */
typedef BYTE TPMI_YES_NO;
/* Table 2:40 - Definition of TPMI_DH_OBJECT Type  */
typedef TPM_HANDLE TPMI_DH_OBJECT;
/* Table 2:41 - Definition of TPMI_DH_PARENT Type  */
typedef TPM_HANDLE TPMI_DH_PARENT;
/* Table 2:42 - Definition of TPMI_DH_PERSISTENT Type  */
typedef TPM_HANDLE TPMI_DH_PERSISTENT;
/* Table 2:43 - Definition of TPMI_DH_ENTITY Type  */
typedef TPM_HANDLE TPMI_DH_ENTITY;
/* Table 2:44 - Definition of TPMI_DH_PCR Type  */
typedef TPM_HANDLE TPMI_DH_PCR;
/* Table 2:45 - Definition of TPMI_SH_AUTH_SESSION Type  */
typedef TPM_HANDLE TPMI_SH_AUTH_SESSION;
/* Table 2:46 - Definition of TPMI_SH_HMAC Type  */
typedef TPM_HANDLE TPMI_SH_HMAC;
/* Table 2:47 - Definition of TPMI_SH_POLICY Type  */
typedef TPM_HANDLE TPMI_SH_POLICY;
/* Table 2:48 - Definition of TPMI_DH_CONTEXT Type  */
typedef TPM_HANDLE TPMI_DH_CONTEXT;
/* Table 49 - Definition of (TPM_HANDLE) TPMI_DH_SAVED Type  */
typedef TPM_HANDLE TPMI_DH_SAVED;
/* Table 2:49 - Definition of TPMI_RH_HIERARCHY Type  */
typedef TPM_HANDLE TPMI_RH_HIERARCHY;
/* Table 2:50 - Definition of TPMI_RH_ENABLES Type  */
typedef TPM_HANDLE TPMI_RH_ENABLES;
/* Table 2:51 - Definition of TPMI_RH_HIERARCHY_AUTH Type  */
typedef TPM_HANDLE TPMI_RH_HIERARCHY_AUTH;
/* Table 2:55 - Definition of TPMI_RH_HIERARCHY_POLICY Type  */
typedef TPM_HANDLE TPMI_RH_HIERARCHY_POLICY;
/* Table 2:52 - Definition of TPMI_RH_PLATFORM Type  */
typedef TPM_HANDLE TPMI_RH_PLATFORM;
/* Table 2:53 - Definition of TPMI_RH_OWNER Type  */
typedef TPM_HANDLE TPMI_RH_OWNER;
/* Table 2:54 - Definition of TPMI_RH_ENDORSEMENT Type  */
typedef TPM_HANDLE TPMI_RH_ENDORSEMENT;
/* Table 2:55 - Definition of TPMI_RH_PROVISION Type  */
typedef TPM_HANDLE TPMI_RH_PROVISION;
/* Table 2:56 - Definition of TPMI_RH_CLEAR Type  */
typedef TPM_HANDLE TPMI_RH_CLEAR;
/* Table 2:57 - Definition of TPMI_RH_NV_AUTH Type  */
typedef TPM_HANDLE TPMI_RH_NV_AUTH;
/* Table 2:58 - Definition of TPMI_RH_LOCKOUT Type  */
typedef TPM_HANDLE TPMI_RH_LOCKOUT;
/* Table 2:59 - Definition of TPMI_RH_NV_INDEX Type  */
typedef TPM_HANDLE TPMI_RH_NV_INDEX;
/* Table 2:60 - Definition of TPMI_RH_AC Type  */
typedef TPM_HANDLE TPMI_RH_AC;
/* Table 2:65 - Definition of TPMI_RH_ACT Type  */
typedef TPM_HANDLE TPMI_RH_ACT;
/* Table 2:61 - Definition of TPMI_ALG_HASH Type  */
typedef TPM_ALG_ID TPMI_ALG_HASH;
/* Table 2:62 - Definition of TPMI_ALG_ASYM Type  */
typedef TPM_ALG_ID TPMI_ALG_ASYM;
/* Table 2:63 - Definition of TPMI_ALG_SYM Type  */
typedef TPM_ALG_ID TPMI_ALG_SYM;
/* Table 2:64 - Definition of TPMI_ALG_SYM_OBJECT Type  */
typedef TPM_ALG_ID TPMI_ALG_SYM_OBJECT;
/* Table 2:65 - Definition of TPMI_ALG_SYM_MODE Type  */
typedef TPM_ALG_ID TPMI_ALG_SYM_MODE;
/* Table 2:66 - Definition of TPMI_ALG_KDF Type  */
typedef TPM_ALG_ID TPMI_ALG_KDF;
/* Table 2:67 - Definition of TPMI_ALG_SIG_SCHEME Type  */
typedef TPM_ALG_ID TPMI_ALG_SIG_SCHEME;
/* Table 2:68 - Definition of TPMI_ECC_KEY_EXCHANGE Type  */
typedef TPM_ALG_ID TPMI_ECC_KEY_EXCHANGE;
/* Table 2:69 - Definition of TPMI_ST_COMMAND_TAG Type  */
typedef TPM_ST TPMI_ST_COMMAND_TAG;
/* Table 2:70 - Definition of TPMI_ALG_MAC_SCHEME Type */
typedef TPM_ALG_ID TPMI_ALG_MAC_SCHEME;
/* le 2:70 - Definition of TPMI_ALG_CIPHER_MODE Type */
typedef TPM_ALG_ID TPMI_ALG_CIPHER_MODE;
/* Table 2:70 - Definition of TPMS_EMPTY Structure  */
typedef BYTE TPMS_EMPTY;

typedef UINT16 TPM_KEY_BITS;
/* Table 2:127 - Definition of TPMI_TDES_KEY_BITS Type  */
typedef TPM_KEY_BITS TPMI_TDES_KEY_BITS;
/* Table 2:127 - Definition of TPMI_AES_KEY_BITS Type  */
typedef TPM_KEY_BITS TPMI_AES_KEY_BITS;
/* Table 2:127 - Definition of TPMI_SM4_KEY_BITS Type  */
typedef TPM_KEY_BITS TPMI_SM4_KEY_BITS;

/* Table 2:158 - Definition of TPMI_ALG_ASYM_SCHEME Type  */
typedef TPM_ALG_ID TPMI_ALG_ASYM_SCHEME;

// Table 2:171 - Definition of TPMI_ALG_ECC_SCHEME Type
typedef TPM_ALG_ID TPMI_ALG_ECC_SCHEME;
// Table 2:172 - Definition of TPMI_ECC_CURVE Type
typedef TPM_ECC_CURVE TPMI_ECC_CURVE;
// Table 2:183 - Definition of TPMI_ALG_PUBLIC Type
typedef TPM_ALG_ID TPMI_ALG_PUBLIC;

#define TPM2B_TYPE(name, bytes)   \
    typedef union                 \
    {                             \
        struct                    \
        {                         \
            UINT16 size;          \
            BYTE buffer[(bytes)]; \
        } t;                      \
        TPM2B b;                  \
    } __attribute__((packed)) TPM2B_##name

// Table 2:33
typedef struct TPMA_LOCALITY {                      
    unsigned    TPM_LOC_ZERO         : 1;
    unsigned    TPM_LOC_ONE          : 1;
    unsigned    TPM_LOC_TWO          : 1;
    unsigned    TPM_LOC_THREE        : 1;
    unsigned    TPM_LOC_FOUR         : 1;
    unsigned    Extended             : 3;
} __attribute__((packed)) TPMA_LOCALITY;



#endif
