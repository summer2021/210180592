#ifndef TPM2_UTILS_H
#define TPM2_UTILS_H

int tpm2_utils_init(void * sub_proc,void * para);
int tpm2_utils_start(void * sub_proc,void * para);

int proc_tpm2utils_input(void *sub_proc, void *recv_msg);

// proceed the tpm2 command
int proc_tpm2utils_GetRandom(void *sub_proc, void *para);
int proc_tpm2utils_PCR_read(void *sub_proc, void *para,int pcrIndex,char *pcrfile);
int proc_tpm2utils_PCR_Extend(void *sub_proc, void *para, int pcrIndex,char *message);
int proc_tpm2utils_Object_CreatePrimary(void *sub_proc, void *para);
int proc_tpm2utils_Object_Create(void *sub_proc, void *para);
int tpm2utils_transmit(int in_len, BYTE *in, int *out_len, BYTE *out);

struct tpm2_utils_init_para
{
     char * channel_name;
}__attribute__((packed));

#endif
