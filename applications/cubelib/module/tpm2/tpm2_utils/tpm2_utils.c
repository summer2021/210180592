/*
 * @Descripttion: 命令测试模块tpm2_utils. 实现根据控制台输入命令字符串生成命令的二进制流 
 * @version: 
 * @Author: 
 * @Date: 
 * @LastEditors: 
 * @LastEditTime: 2021-07-15 10:27:47
 */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <dirent.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
//#include <netinet/in.h>
//#include <arpa/inet.h>
#include <string.h>
#include "data_type.h"
#include <errno.h>
#include "alloc.h"
#include "list.h"
//#include "attrlist.h"
#include "memfunc.h"
#include "basefunc.h"
#include "../../../include/struct_deal.h"
#include "crypto_func.h"
#include "../../../include/memdb.h"
#include "../../../include/message.h"

#include "../include/tpm2_value.h"
#include "../include/tpm2_base_structs.h"
#include "../include/tpm2_structure.h"

#include "../include/tpm2_global.h"
#include "../include/tpm2_command.h"
// #include "vtcm_alg.h"

#include "tpm2_utils.h"

static BYTE Buf[DIGEST_SIZE * 32];
static BYTE Output[DIGEST_SIZE * 32];

void *curr_recv_msg = RT_NULL;


int proc_tpm2utils_input(void *sub_proc, void *recv_msg);

// proceed the tpm2 command
int proc_tpm2utils_Object_CreatePrimary(void *sub_proc, void *para);
int tpm2utils_transmit(int in_len, BYTE *in, int *out_len, BYTE *out);

int tpm2_utils_init(void *sub_proc, void *para)
{
    printf("tpm2_utils_init module start!\n");
    int ret;
    struct tpm2_utils_init_para *init_para = para;
    if (para == RT_NULL)
    {
        return -EINVAL;
    }

    return 0;
}
void print_bin_data(BYTE * data,int len,int width)
{
    int i;
    for(i=0;i<len;i++){
        printf("%.2x ",data[i]);
        if (width>0)
        {
            if((i+1)%width==0)
                printf("\n");
        }
    }
    printf("\n");
}

int proc_tpm2utils_GetRandom(void *sub_proc, void *para)
{
    int outlen;
    int i = 0;
    int ret = 0;
    void *tpm2_template;
    RECORD(TPM2_IN, GetRandom) * tpm2_input;
    RECORD(TPM2_OUT, GetRandom) * tpm2_output;
    tpm2_input = rt_malloc(sizeof(*tpm2_input));
    if (tpm2_input == RT_NULL)
        return -ENOMEM;
    tpm2_output = rt_malloc(sizeof(*tpm2_output));
    if (tpm2_output == RT_NULL)
        return -ENOMEM;
    tpm2_input->tag = TPM_ST_NO_SESSIONS;
    tpm2_input->commandCode = TPM_CC_GetRandom;
    tpm2_input->bytesRequested = 0x10;
    tpm2_template = memdb_get_template(TYPE_PAIR(TPM2_IN, GetRandom));
    if (tpm2_template == RT_NULL)
        return -EINVAL;
    tpm2_input->commandSize = sizeof(*tpm2_input);
    ret = struct_2_blob(tpm2_input, Buf, tpm2_template);
    if (ret < 0)
        return ret;
    printf("Send command for getRandom:\n");
    print_bin_data(Buf, ret, 8);

    ret = tpm2utils_transmit(tpm2_input->commandSize, Buf, &outlen, Buf);
    if (ret < 0)
        return ret;
    printf("Receive  output is:\n");
    print_bin_data(Buf, outlen, 8);

    tpm2_template = memdb_get_template(TYPE_PAIR(TPM2_OUT, GetRandom));
    ret = blob_2_struct(Buf, tpm2_output, tpm2_template);
    if (ret < 0)
        return ret;

    tpm2_template = memdb_get_template(TYPE_PAIR(TPM2_OUT, GetRandom));
    struct_read_elem_text("randomBytes.buffer", tpm2_output, Output, tpm2_template);

    sprintf(Buf, "%d %d %s\n", tpm2_output->responseCode, tpm2_output->randomBytes.b.size, Output);
    printf("Output para: %s\n", Buf);
   
    return ret;
}

int proc_tpm2utils_PCR_Extend(void *sub_proc, void *para, int pcrIndex,char *message)
{
    int outlen;
    int i = 0;
    int ret = 0;
    int cmdsize = 0;
    // int pcrIndex;
    // char *message = RT_NULL;
    void *tpm2_template;
    RECORD(TPM2_IN, PCR_Extend) * tpm2_input;
    RECORD(TPM2_OUT, PCR_Extend) * tpm2_output;
    tpm2_input = rt_malloc(sizeof(*tpm2_input));
    if (tpm2_input == RT_NULL)
        return -ENOMEM;
    tpm2_output = rt_malloc(sizeof(*tpm2_output));
    if (tpm2_output == RT_NULL)
        return -ENOMEM;

    // struct tpm2_utils_input *input_para = para; //定义输入参数
    // char *index_para;
    // char *value_para;
    // if ((input_para->param_num > 0) && (input_para->param_num % 2 == 1))
    // {
    //     for (i = 1; i < input_para->param_num; i += 2)
    //     {
    //         index_para = input_para->params + i * DIGEST_SIZE;
    //         value_para = index_para + DIGEST_SIZE; //input_para->params==extend   index_para=-ix   value_para=2 ？？
    //         if (!Strcmp("-ix", index_para))        //in: extend -ix 2 -ic aaaa
    //         {
    //             sscanf(value_para, "%x", &pcrIndex); //tpm2_input->pcrIndex未定义？
    //         }
    //         else if (!Strcmp("-ic", index_para))
    //         {
    //             message = value_para;
    //         }
    //         else
    //         {
    //             printf("Error cmd format! should be %s -ix pcrindex -wf pcrfile", input_para->params);
    //             return -EINVAL;
    //         }
    //     }
    // }

    tpm2_input->tag = TPM_ST_SESSIONS;
    tpm2_input->commandCode = TPM_CC_PCR_Extend;
    tpm2_input->pcrHandle = pcrIndex;
    tpm2_input->digests.count = 1;
    tpm2_input->digests.digests = rt_malloc(sizeof(TPM2B_DIGEST) * tpm2_input->digests.count);
    tpm2_input->digests.digests->b.buffer = rt_malloc(sizeof(TPM2B_DIGEST) * tpm2_input->digests.count);
    tpm2_input->digests.digests[0].b.size = sizeof(*message);
    tpm2_input->digests.digests[0].b.buffer = rt_malloc(tpm2_input->digests.digests[0].b.size);
    rt_memset(tpm2_input->digests.digests[0].b.buffer, (int)(*message), tpm2_input->digests.digests[0].b.size); //把数据S写入缓冲区
    // tpm2_input->digests.digests[0].b.buffer = message;

    tpm2_template = memdb_get_template(TYPE_PAIR(TPM2_IN, PCR_Extend));
    if (tpm2_template == RT_NULL)
        return -EINVAL;
    tpm2_input->commandSize = sizeof(*tpm2_input);
    cmdsize = struct_2_blob(tpm2_input, Buf, tpm2_template); //?将命令转化为2进制格式？ Buf是个缓冲区 用来存放什么
    if (cmdsize < 0)
        return cmdsize;
    tpm2_input->commandSize = cmdsize;
    *(int *)(Buf + sizeof(tpm2_input->tag)) = htonl(cmdsize); //htonl()的作用是什么？将主机字节顺序转换为网络字节顺序
                                                              //  	cmdsize = struct_2_blob(tpm2_input,Buf,tpm2_template);
                                                              // 	if(cmdsize<0)
                                                              //  		return cmdsize;
    printf("Send command for PCR_Extend:\n");
    print_bin_data(Buf, cmdsize, 8);
    ret = tpm2utils_transmit(cmdsize, Buf, &outlen, Buf);
    if (ret < 0)
        return ret;
    return ret;
}

int proc_tpm2utils_PCR_read(void *sub_proc, void *para,int pcrIndex,char *pcrfile)
{
    int outlen;
    int i = 0;
    int ret = 0;
    int cmdsize = 0;
    // int pcrIndex;
    void *tpm2_template;
    // char *pcrfile;

    RECORD(TPM2_IN, PCR_Read) * tpm2_input;
    RECORD(TPM2_OUT, PCR_Read) * tpm2_output;
    tpm2_input = rt_malloc(sizeof(*tpm2_input));
    if (tpm2_input == RT_NULL)
        return -ENOMEM;
    tpm2_output = rt_malloc(sizeof(*tpm2_output));
    if (tpm2_output == RT_NULL)
        return -ENOMEM;

    // struct tpm2_utils_input *input_para = para; //定义输入参数
    // char *index_para;
    // char *value_para;
    // if ((input_para->param_num > 0) && (input_para->param_num % 2 == 1)) //余1是什么意思
    // {
    //     for (i = 1; i < input_para->param_num; i += 2)
    //     {
    //         index_para = input_para->params + i * DIGEST_SIZE;
    //         value_para = index_para + DIGEST_SIZE; //input_para->params==pcrread   index_para=-ix   value_para=2 ？？
    //         if (!Strcmp("-ix", index_para))        //in:  pcrread -ix 2 -wf pcrfile
    //         {
    //             sscanf(value_para, "%x", &pcrIndex); //tpm2_input->pcrIndex未定义？
    //         }
    //         else if (!Strcmp("-wf", index_para))
    //         {
    //             pcrfile = value_para;
    //         }
    //         else
    //         {
    //             printf("Error cmd format! should be %s -ix pcrindex -wf pcrfile", input_para->params);
    //             return -EINVAL;
    //         }
    //     }
    // }
    //输入消息初始化
    tpm2_input->tag = TPM_ST_SESSIONS;
    tpm2_input->commandCode = TPM_CC_PCR_Read;
    tpm2_input->pcrSelectionIn.count = 1;                                                                              //指定pcr查询列表中的pcr种类数，根据hash划分？？？？
    tpm2_input->pcrSelectionIn.pcrSelections = rt_malloc(sizeof(TPMS_PCR_SELECTION) * tpm2_input->pcrSelectionIn.count); //为pcr列表开辟空间,TPMS_PCR_SELECTION
    tpm2_input->pcrSelectionIn.pcrSelections[0].hash = TPM_ALG_SM3_256;                                                //采用的哈希函数
    tpm2_input->pcrSelectionIn.pcrSelections[0].sizeofSelect = 3;                                                      //pcrSelect数组中的八位字节个数，24个pcr需要3个八位字节
    //tpm2_input->pcrSelectionIn.pcrSelections[0].pcrSelect=(char *)rt_malloc(tpm2_input->pcrSelectionIn.pcrSelections[0].sizeofSelect*sizeof(char)); /**为pcr查询数组开辟空间**/
    tpm2_input->pcrSelectionIn.pcrSelections[0].pcrSelect = rt_malloc(tpm2_input->pcrSelectionIn.pcrSelections[0].sizeofSelect);  //为pcr查询数组开辟空间
    rt_memset(tpm2_input->pcrSelectionIn.pcrSelections[0].pcrSelect, 0, tpm2_input->pcrSelectionIn.pcrSelections[0].sizeofSelect); //初始化查询数组 三个八位字节全部为0
    //pcrSelect[0]&=0x01;/**如何将想要读取的pcr号对应的位赋值为1？？？位图置位函数在cube-1.3/cubelib/include/memfunc.h里**/
    bitmap_set(tpm2_input->pcrSelectionIn.pcrSelections[0].pcrSelect, pcrIndex); /**将pcr下标对应的位赋值为1**/
    //int bitmap_clear(BYTE * bitmap,int site);
    //int bitmap_get(BYTE * bitmap,int site);
    //int bitmap_is_allset(BYTE * bitmap,int size);
    tpm2_template = memdb_get_template(TYPE_PAIR(TPM2_IN, PCR_Read)); //获取tpm2_input的模板，用模板直接实现结构的序列化。
    if (tpm2_template == RT_NULL)
        return -EINVAL;
    tpm2_input->commandSize = sizeof(*tpm2_input);

    cmdsize = struct_2_blob(tpm2_input, Buf, tpm2_template); //消息序列化
    if (cmdsize < 0)
        return cmdsize;
    tpm2_input->commandSize = cmdsize;

    *(int *)(Buf + sizeof(tpm2_input->tag)) = htonl(cmdsize); //将主机字节顺序转换为网络字节顺序

    printf("Send command for PCR_Read:\n");
    print_bin_data(Buf, cmdsize, 8); //以16进制显示发送的二进制消息

    ret = tpm2utils_transmit(cmdsize, Buf, &outlen, Buf); //通过通道与驱动对接，到达模拟器，最终与pcr模块传输数据
    if (ret < 0)
        return ret;
    return 0;
}

int proc_tpm2utils_Object_CreatePrimary(void *sub_proc, void *para)
{
    printf("proc_tpm2utils_Object_CreatePrimary start :\n");
    int outlen;
    int i = 0;
    int ret = 0;
    int cmdsize = 0;
    int pcrIndex;
    void *tpm2_template;
    char *pcrfile;

    RECORD(TPM2_IN, CreatePrimary) * tpm2_input;
    RECORD(TPM2_OUT, CreatePrimary) * tpm2_output;
    tpm2_input = rt_malloc(sizeof(*tpm2_input));
    if (tpm2_input == RT_NULL)
        return -ENOMEM;
    tpm2_output = rt_malloc(sizeof(*tpm2_output));
    if (tpm2_output == RT_NULL)
        return -ENOMEM;

    struct tpm2_utils_input *input_para = para; //定义输入参数
    char *index_para;
    char *value_para;
    //输入消息初始化
    //对命令基本信息初始化
    tpm2_input->tag = TPM_ST_SESSIONS;
    tpm2_input->commandCode = TPM_CC_CreatePrimary;

    //对primaryHandle初始化. TCM_CreatePrimary命令用于在一个主种子下创建一个主对象，或在TPM_RH_NULL下创建一个临时对象。
    tpm2_input->primaryHandle = TPM_RH_NULL;

    //对公共模板inPublic初始化
    tpm2_input->inPublic.publicArea.type = TPM_ALG_ECC; //  先采用类型为：TPM_ALG_ECC椭圆曲线加密算法SM2
    //  -----   参考 part3_Commands : 12.1.1  b)  If the Object is an asymmetric key:
    tpm2_input->inPublic.publicArea.nameAlg = TPM_ALG_SM2; // nameAlg 设置为 SM2 非对称秘钥
    //  -----   对于TPMA_OBJECT类型的 objectAttributes来说：
    //  -----   如果调用者表示他们已经提供了数据，那么要确保他们已经提供了一些数据。
    //  -----   当'sensitive.data'是一个空缓冲区时，sensitiveDataOrigin是CLEAR，或者当'sensitive.data'不是空的时候是SET。
    //  -----   对于普通对象来说，只有当sensitiveDataOrigin为CLEAR时才能提供数据。
    //  -----   Asymmetric keys cannot have the sensitive portion provided. 非对称秘钥不能提供敏感部分。
    // TODO: objectAttributes属性设置有问题
#if USE_BIT_FIELD_STRUCTURES
    tpm2_input->inPublic.publicArea.objectAttributes.sensitiveDataOrigin = CLEAR;
#else  // USE_BIT_FIELD_STRUCTURES
    tpm2_input->inPublic.publicArea.objectAttributes = TPMA_ZERO_INITIALIZER();
    // tpm2_input->inPublic.publicArea.objectAttributes = TPMA_OBJECT_INITIALIZER(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    CLEAR_ATTRIBUTE(tpm2_input->inPublic.publicArea.objectAttributes, TPMA_OBJECT, sensitiveDataOrigin);
#endif // USE_BIT_FIELD_STRUCTURES

    //  -----   对 authPolicy 初始化
    // TODO: authPolicy
    tpm2_input->inPublic.publicArea.authPolicy.b.buffer = RT_NULL;
    tpm2_input->inPublic.publicArea.authPolicy.b.size = 0;
    //  -----   对 parameters 初始化
    //  -----   parameters参数中，若type为 TPM_ALG_ECC ，设置parameters.eccDetaill
    tpm2_input->inPublic.publicArea.parameters.eccDetail.symmetric.algorithm = TPM_ALG_NULL;
    tpm2_input->inPublic.publicArea.parameters.eccDetail.symmetric.keyBits.sym = TPM_ALG_NULL;
    tpm2_input->inPublic.publicArea.parameters.eccDetail.symmetric.mode.sym = TPM_ALG_NULL;
    tpm2_input->inPublic.publicArea.parameters.eccDetail.scheme.scheme = TPM_ALG_SM2;
    // TODO: .sm2.hashAlg
    tpm2_input->inPublic.publicArea.parameters.eccDetail.scheme.details.sm2.hashAlg = TPM_ALG_SM2;
    tpm2_input->inPublic.publicArea.parameters.eccDetail.curveID = TPM_ECC_SM2_P256;
    tpm2_input->inPublic.publicArea.parameters.eccDetail.kdf.scheme = TPM_ALG_NULL; // 目前没有任何命令可以让调用者指定用于ECC解密密钥的KDF。由于没有使用这种能力，参考实现要求将模板中的kdf设置为TPM_ALG_NULL，否则将返回TPM_RC_KDF。
    // kdf设置为TPM_ALG_NULL，detail没有设置
    // tpm2_input->inPublic.publicArea.parameters.eccDetail.kdf.details.kdf1_sp800_56a.hashAlg = TPM_ALG_SM2;
    //  -----   parameters参数中，若type为 TPM_ALG_ECC ，设置parameters.asymDetail
    tpm2_input->inPublic.publicArea.parameters.asymDetail.symmetric.algorithm = TPM_ALG_SM2;
    tpm2_input->inPublic.publicArea.parameters.asymDetail.symmetric.keyBits.sym = TPM_ALG_NULL;
    tpm2_input->inPublic.publicArea.parameters.asymDetail.symmetric.mode.sym = TPM_ALG_NULL;
    tpm2_input->inPublic.publicArea.parameters.asymDetail.scheme.scheme = TPM_ALG_SM2;
    // TODO: .sm2.hashAlg
    tpm2_input->inPublic.publicArea.parameters.asymDetail.scheme.details.sm2.hashAlg = TPM_ALG_SM2;
    tpm2_input->inPublic.publicArea.parameters.asymDetail.scheme.details.anySig.hashAlg = TPM_ALG_SM2;
    // TODO: unique
    // tpm2_input->inPublic.publicArea.unique.ecc = 0;
    // tpm2_input->inPublic.publicArea.unique.derive = 0;
    tpm2_input->inPublic.size = sizeof(tpm2_input->inPublic.publicArea);

    //对敏感区域inSensitive初始化
    // userAuth USER认证的秘密值
    tpm2_input->inSensitive.sensitive.userAuth.b.buffer = RT_NULL;
    tpm2_input->inSensitive.sensitive.userAuth.b.size = 0;
    tpm2_input->inSensitive.sensitive.data.b.buffer = RT_NULL;
    tpm2_input->inSensitive.sensitive.data.b.size = 0;
    // 敏感字节的大小(可能不是零)。这个缓冲区中的userAuth和data参数可能都是零长度，但这个参数的最小大小将是TPMS_SENSITIVE_CREATE的两个参数的size字段的和。
    tpm2_input->inSensitive.size = tpm2_input->inSensitive.sensitive.userAuth.b.size + tpm2_input->inSensitive.sensitive.data.b.size;
    // tpm2_input->inSensitive.size = sizeof(tpm2_input->inSensitive.sensitive);

    // 对创建数据 outsideInfo 初始化
    tpm2_input->outsideInfo.b.buffer = RT_NULL;
    tpm2_input->outsideInfo.b.size = 0;

    //对creationPCR初始化
    tpm2_input->creationPCR.count = 1;                                                                           //指定pcr查询列表中的pcr种类数，根据hash划分
    tpm2_input->creationPCR.pcrSelections = rt_malloc(sizeof(TPMS_PCR_SELECTION) * tpm2_input->creationPCR.count); //为pcr列表开辟空间,TPMS_PCR_SELECTION
    tpm2_input->creationPCR.pcrSelections[0].hash = TPM_ALG_SM3_256;                                             //采用的哈希函数
    tpm2_input->creationPCR.pcrSelections[0].sizeofSelect = 3;                                                   //pcrSelect数组中的八位字节个数，24个pcr需要3个八位字节
    //tpm2_input->creationPCR.pcrSelections[0].pcrSelect=(char *)rt_malloc(tpm2_input->creationPCR.pcrSelections[0].sizeofSelect*sizeof(char)); /**为pcr查询数组开辟空间**/
    tpm2_input->creationPCR.pcrSelections[0].pcrSelect = rt_malloc(sizeof(BYTE) * tpm2_input->creationPCR.pcrSelections[0].sizeofSelect); //为pcr查询数组开辟空间
    rt_memset(tpm2_input->creationPCR.pcrSelections[0].pcrSelect, 0, tpm2_input->creationPCR.pcrSelections[0].sizeofSelect);               //初始化查询数组 三个八位字节全部为0
    //pcrSelect[0]&=0x01;/**如何将想要读取的pcr号对应的位赋值为1？？？位图置位函数在cube-1.3/cubelib/include/memfunc.h里**/
    bitmap_set(tpm2_input->creationPCR.pcrSelections[0].pcrSelect, pcrIndex); /**将pcr下标对应的位赋值为1**/
    //int bitmap_clear(BYTE * bitmap,int site);
    //int bitmap_get(BYTE * bitmap,int site);
    //int bitmap_is_allset(BYTE * bitmap,int size);
    tpm2_template = memdb_get_template(TYPE_PAIR(TPM2_IN, CreatePrimary)); //获取tpm2_input的模板，用模板直接实现结构的序列化。
    if (tpm2_template == RT_NULL)
    {
        return -EINVAL;
    }
    tpm2_input->commandSize = sizeof(*tpm2_input);

    //开始消息序列化commandSize
    cmdsize = struct_2_blob(tpm2_input, Buf, tpm2_template);
    if (cmdsize < 0)
    {
        return cmdsize;
    }
    //重新设置消息序列化后的commandSize
    tpm2_input->commandSize = cmdsize;

    //将主机字节顺序转换为网络字节顺序
    *(int *)(Buf + sizeof(tpm2_input->tag)) = htonl(cmdsize);

    printf("Send command for CreatePrimary:\n");
    print_bin_data(Buf, cmdsize, 8); //以16进制显示发送的二进制命令消息

    //通过通道与驱动对接，到达模拟器，最终与pcr模块传输数据
    ret = tpm2utils_transmit(cmdsize, Buf, &outlen, Buf);
    if (ret < 0)
        return ret;
    return 0;
}

int tpm2utils_transmit(int in_len, BYTE *in, int *out_len, BYTE *out)
{
    int ret = 0;

    return ret;
}
