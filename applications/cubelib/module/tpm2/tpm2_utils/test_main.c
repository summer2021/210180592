#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include "../include/errno.h"
#include "../../../include/data_type.h"
#include "../../../include/alloc.h"
#include "../../../include/basefunc.h"
#include "../../../include/struct_deal.h"
#include "../../../include/memdb.h"
#include "../../../include/message.h"
#include "tpm2_utils.h"

int tpm2_utils_test_read_json_file(char *file_name)
{
	int ret;

	int fd;
	int readlen;
	int json_offset;

	int struct_no = 0;
	void *root_node;
	void *findlist;
	void *memdb_template;
	BYTE uuid[DIGEST_SIZE];
	char json_buffer[140960];

	fd = open(file_name, O_RDONLY);
	if (fd < 0)
		return fd;

	readlen = read(fd, json_buffer, 140960);
	if (readlen < 0)
		return -EIO;
	json_buffer[readlen] = 0;
	rt_kprintf("%s\n", json_buffer);
	close(fd);

	json_offset = 0;
	while (json_offset < readlen)
	{
		ret = json_solve_str(&root_node, json_buffer + json_offset);
		if (ret < 0)
		{
			rt_kprintf("solve json str error!\n");
			break;
		}
		json_offset += ret;
		if (ret < 32)
			continue;

		ret = memdb_read_desc(root_node, uuid);
		if (ret < 0)
			break;
		struct_no++;
	}

	return struct_no;
}


int test_CreatePrimary()
{
//	char json_buffer[4096];
//	char print_buffer[4096];
//	BYTE bin_buffer[4096];
	int ret;
//	int readlen;
//	int json_offset;
//	void *root_node;
//	void *findlist;
//	void *memdb_template;
//	BYTE uuid[DIGEST_SIZE];
//	int i;
//
//	char *baseconfig[] =
//		{
//			"namelist.json",
//			"typelist.json",
//			"subtypelist.json",
//			"memdb.json",
//			"msghead.json",
//			"headrecord.json",
//			"tpm2_script.json",
//			"tpm2_value.json",
//			"tpm2_general_define.json",
//			"tpm2_command_define.json",
//			RT_NULL};
//	unsigned char *alloc_buffer;
//	alloc_buffer = malloc(4096 * (256 + 1));
//	if ((UINT64)alloc_buffer % 4096 == 0)
//	{
//		alloc_init(alloc_buffer, 256);
//	}
//	else
//	{
//		alloc_init(alloc_buffer + 4096 - (UINT64)alloc_buffer % 4096, 256);
//	}
//	//	alloc_init(alloc_buffer);
//	struct_deal_init();
//	memdb_init();
//
//	for (i = 0; baseconfig[i] != RT_NULL; i++)
//	{
//		ret = tpm2_utils_test_read_json_file(baseconfig[i]);
//		if (ret < 0)
//			return ret;
//		rt_kprintf("read %d elem from file %s!\n", ret, baseconfig[i]);
//	}
//
//	rt_kprintf("*************************************************************************\n");

	void *ex_module;
	ret = proc_tpm2utils_Object_CreatePrimary(ex_module, RT_NULL);
//	free(alloc_buffer);
	return 0;
}
int test_GetRandom()
{
	char json_buffer[4096];
	char print_buffer[4096];
	BYTE bin_buffer[4096];
	int ret;
	int readlen;
	int json_offset;
	void *root_node;
	void *findlist;
	void *memdb_template;
	BYTE uuid[DIGEST_SIZE];
	int i;

	char *baseconfig[] =
		{
			"namelist.json",
			"typelist.json",
			"subtypelist.json",
			"memdb.json",
			"msghead.json",
			"headrecord.json",
			"tpm2_script.json",
			"tpm2_value.json",
			"tpm2_general_define.json",
			"tpm2_command_define.json",
			RT_NULL};
	unsigned char *alloc_buffer;
	alloc_buffer = rt_malloc(4096 * (256 + 1));
	if ((UINT64)alloc_buffer % 4096 == 0)
	{
		alloc_init(alloc_buffer, 256);
	}
	else
	{
		alloc_init(alloc_buffer + 4096 - (UINT64)alloc_buffer % 4096, 256);
	}
	//	alloc_init(alloc_buffer);
	struct_deal_init();
	memdb_init();

	for (i = 0; baseconfig[i] != RT_NULL; i++)
	{
		ret = tpm2_utils_test_read_json_file(baseconfig[i]);
		if (ret < 0)
			return ret;
		rt_kprintf("read %d elem from file %s!\n", ret, baseconfig[i]);
	}

	rt_kprintf("*************************************************************************\n");

	

	void *ex_module;
	ret = proc_tpm2utils_GetRandom(ex_module, RT_NULL);
	free(alloc_buffer);
	return 0;
}

int test_PCR_read()
{
	char json_buffer[4096];
	char print_buffer[4096];
	BYTE bin_buffer[4096];
	int ret;
	int readlen;
	int json_offset;
	void *root_node;
	void *findlist;
	void *memdb_template;
	BYTE uuid[DIGEST_SIZE];
	int i;

	char *baseconfig[] =
		{
			"namelist.json",
			"typelist.json",
			"subtypelist.json",
			"memdb.json",
			"msghead.json",
			"headrecord.json",
			"tpm2_script.json",
			"tpm2_value.json",
			"tpm2_general_define.json",
			"tpm2_command_define.json",
			RT_NULL};
	unsigned char *alloc_buffer;
	alloc_buffer = malloc(4096 * (256 + 1));
	if ((UINT64)alloc_buffer % 4096 == 0)
	{
		alloc_init(alloc_buffer, 256);
	}
	else
	{
		alloc_init(alloc_buffer + 4096 - (UINT64)alloc_buffer % 4096, 256);
	}
	//	alloc_init(alloc_buffer);
	struct_deal_init();
	memdb_init();

	for (i = 0; baseconfig[i] != RT_NULL; i++)
	{
		ret = tpm2_utils_test_read_json_file(baseconfig[i]);
		if (ret < 0)
			return ret;
		rt_kprintf("read %d elem from file %s!\n", ret, baseconfig[i]);
	}

	rt_kprintf("*************************************************************************\n");

	

	void *ex_module;
	ret = proc_tpm2utils_PCR_read(ex_module, RT_NULL,1,"pcrfile");
	free(alloc_buffer);
	return 0;
}
int test_PCR_Extend()
{
	char json_buffer[4096];
	char print_buffer[4096];
	BYTE bin_buffer[4096];
	int ret;
	int readlen;
	int json_offset;
	void *root_node;
	void *findlist;
	void *memdb_template;
	BYTE uuid[DIGEST_SIZE];
	int i;

	char *baseconfig[] =
		{
			"namelist.json",
			"typelist.json",
			"subtypelist.json",
			"memdb.json",
			"msghead.json",
			"headrecord.json",
			"tpm2_script.json",
			"tpm2_value.json",
			"tpm2_general_define.json",
			"tpm2_command_define.json",
			RT_NULL};
	unsigned char *alloc_buffer;
	alloc_buffer = malloc(4096 * (256 + 1));
	if ((UINT64)alloc_buffer % 4096 == 0)
	{
		alloc_init(alloc_buffer, 256);
	}
	else
	{
		alloc_init(alloc_buffer + 4096 - (UINT64)alloc_buffer % 4096, 256);
	}
	//	alloc_init(alloc_buffer);
	struct_deal_init();
	memdb_init();

	for (i = 0; baseconfig[i] != RT_NULL; i++)
	{
		ret = tpm2_utils_test_read_json_file(baseconfig[i]);
		if (ret < 0)
			return ret;
		rt_kprintf("read %d elem from file %s!\n", ret, baseconfig[i]);
	}

	rt_kprintf("*************************************************************************\n");

	

	void *ex_module;
	ret = proc_tpm2utils_PCR_Extend(ex_module, RT_NULL,1,"aaa");
	free(alloc_buffer);
	return 0;
}


int tpm2_utils_test_main()
{
	char json_buffer[4096];
	char print_buffer[4096];
	BYTE bin_buffer[4096];
	int ret;
	int readlen;
	int json_offset;
	void *root_node;
	void *findlist;
	void *memdb_template;
	BYTE uuid[DIGEST_SIZE];
	int i;

	char *baseconfig[] =
		{
			"namelist.json",
			"typelist.json",
			"subtypelist.json",
			"memdb.json",
			"msghead.json",
			"headrecord.json",
			"tpm2_script.json",
			"tpm2_value.json",
			"tpm2_general_define.json",
			"tpm2_command_define.json",
			RT_NULL};
	unsigned char *alloc_buffer;
	alloc_buffer = malloc(4096 * (256 + 1));
	if ((UINT64)alloc_buffer % 4096 == 0)
	{
		alloc_init(alloc_buffer, 256);
	}
	else
	{
		alloc_init(alloc_buffer + 4096 - (UINT64)alloc_buffer % 4096, 256);
	}
	//	alloc_init(alloc_buffer);
	struct_deal_init();
	memdb_init();

	for (i = 0; baseconfig[i] != RT_NULL; i++)
	{
		ret = tpm2_utils_test_read_json_file(baseconfig[i]);
		if (ret < 0)
			return ret;
		rt_kprintf("read %d elem from file %s!\n", ret, baseconfig[i]);
	}

	rt_kprintf("*************************************************************************\n");

	// void *message;
	// struct basic_message base_msg;
	// void *recv_msg;
	// message = message_create(TYPE(TPM2_UTILS), SUBTYPE(TPM2_UTILS, INPUT), recv_msg);

	// 输出内存相关记录
	// void *record;

	// record = memdb_get_first(DB_NAMELIST, 0);
	// while (record != RT_NULL)
	// {
	// 	ret = memdb_print(record, print_buffer);
	// 	if (ret < 0)
	// 		return -EINVAL;
	// 	rt_kprintf("%s\n", print_buffer);
	// 	record = memdb_get_next(DB_NAMELIST, 0);
	// }

	// record = memdb_get_first(DB_TYPELIST, 0);
	// while (record != RT_NULL)
	// {
	// 	ret = memdb_print(record, print_buffer);
	// 	if (ret < 0)
	// 		return -EINVAL;
	// 	rt_kprintf("%s\n", print_buffer);
	// 	record = memdb_get_next(DB_TYPELIST, 0);
	// }

	// record = memdb_get_first(DB_STRUCT_DESC, 0);
	// while (record != RT_NULL)
	// {
	// 	ret = memdb_print(record, print_buffer);
	// 	if (ret < 0)
	// 		return -EINVAL;
	// 	rt_kprintf("%s\n", print_buffer);
	// 	record = memdb_get_next(DB_STRUCT_DESC, 0);
	// }
	// // 输出内存消息
	// int msg_type = memdb_get_typeno("MESSAGE");
	// if (msg_type <= 0)
	// 	return -EINVAL;

	// int subtype = memdb_get_subtypeno(msg_type, "HEAD");
	// if (subtype <= 0)
	// 	return -EINVAL;

	// record = memdb_get_first(msg_type, subtype);
	// while (record != RT_NULL)
	// {
	// 	ret = memdb_print(record, print_buffer);
	// 	if (ret < 0)
	// 		return -EINVAL;
	// 	rt_kprintf("%s\n", print_buffer);
	// 	record = memdb_get_next(msg_type, subtype);
	// }

	void *ex_module;
	ret = proc_tpm2utils_Object_CreatePrimary(ex_module, RT_NULL);
	free(alloc_buffer);


	// test_CreatePrimary();
	// test_GetRandom();
	test_PCR_read();
	// test_PCR_Extend();
	return 0;
}
MSH_CMD_EXPORT(test_CreatePrimary, test_CreatePrimary)
MSH_CMD_EXPORT(test_GetRandom, test_GetRandom)
MSH_CMD_EXPORT(test_PCR_read, test_PCR_read)
MSH_CMD_EXPORT(test_PCR_Extend, test_PCR_Extend)
