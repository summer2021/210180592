/**
 *
 * File: cube_static.h
 *
 */

#ifndef CUBE_DYNAMIC_H_
#define CUBE_DYNAMIC_H_

#define CUBE_DMEM_EMPTY 0x00
#define CUBE_DMEM_ELEM 0x01
#define CUBE_DMEM_OBJECT 0x02
#define CUBE_DMEM_ZOMBIE 0x03

/******************************************************************************
 *
 * Definitions
 *
 ******************************************************************************/

struct dmem_sys
{
	UINT32 total_size;  // total dmem size
	UINT16 pages_num;  // dmem pages num
	UINT16 first_page;  //first dmem page
	UINT16 curr_page; // current dmem page
	UINT16 curr_offset; // current dmem page
	UINT32 half_stone;  // 
	UINT32 quad_stone;  // 
}__attribute__((packed));

#define DMEM_FLAG_MASK 0xf000
#define DMEM_SIZE_MASK 0x0fff

#define DMEM_GET_SIZE(pointer)    (((struct dmem_head *)((BYTE *)pointer - sizeof(struct dmem_head)))->dmem_attr & DMEM_SIZE_MASK) 
#define DMEM_GET_FLAG(pointer)    ((((struct dmem_head *)((BYTE *)pointer - sizeof(struct dmem_head)))->dmem_attr & DMEM_FLAG_MASK)>>12) 

#define DMEM_SET_SIZE(pointer,size)    (((struct dmem_head *)((BYTE *)pointer - sizeof(struct dmem_head)))->dmem_attr &= ~DMEM_SIZE_MASK | size) 
#define DMEM_SET_FLAG(pointer,flag)    (((struct dmem_head *)((BYTE *)pointer - sizeof(struct dmem_head)))->dmem_attr &= ~DMEM_FLAG_MASK|(flag<<12)) 
#endif /* CUBE_DYNAMIC_H_ */
