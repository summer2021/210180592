#include "../include/errno.h"
#include "../include/data_type.h"
#include "../include/alloc.h"
#include "../include/memfunc.h"
#include "alloc_init.h"
#include "cube_static.h"

void * Malloc(int size, void * pointer){
    int page;
    page = addr_get_page(pointer);
    if(pointer == RT_NULL)
        return -EINVAL;
    if(pages[page].type == TEMP_PAGE)
        return Salloc(size);
    else if(pages[page].type == STATIC_PAGE || pages[page].type == CACHE_PAGE || pages[page].type == DYNAMIC_PAGE)
        return Dalloc(size,pointer);
    else
        return -EINVAL;
}
