/**
 * Copyright [2015] Tianfu Ma (matianfu@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File: buddy.c
 *
 * Created on: Jun 5, 2015
 * Author: Tianfu Ma (matianfu@gmail.com)
 */

#include "../include/errno.h"
#include "../include/data_type.h"
#include "../include/alloc.h"
#include "../include/memfunc.h"
#include "alloc_init.h"
#include "cube_buddy.h"

extern BYTE * first_page;
static void ** listarray;
static buddy_t * buddy_struct;
static struct temp_mem_sys * temp_mem_struct;
extern struct page_index *pages;
extern struct alloc_segment_address * root_address;

void *  buddy_struct_init (int order, void * addr);

void * temp_memory_init(int order)
{
	void * curr_pointer;
	curr_pointer=get_firstpagemem_bottom(sizeof(struct temp_mem_sys));
	if(curr_pointer == RT_NULL)
		return RT_NULL;
	root_address->temp_area=(UINT16)(curr_pointer-(void *)first_page);
	temp_mem_struct=curr_pointer;
	curr_pointer = buddy_struct_init(order,first_page+PAGE_SIZE);
	return curr_pointer;
}

void * buddy_struct_init (int order, void * addr)
{
	int buddy_manager_size;

	if((order<PAGE_ORDER) || (order >=MAX_ORDER))
        return RT_NULL;
	if(((int)(addr-(void *)first_page))%PAGE_SIZE!=0)
		    return RT_NULL;
	// get buddy_manager's site	
	buddy_manager_size= sizeof(*buddy_struct)+order*sizeof(void *)+sizeof(void *)*2;
	
	buddy_struct = (buddy_t *)get_firstpagemem_upper(buddy_manager_size);

	// empty the buddy_struct
	rt_memset(buddy_struct,0,sizeof(*buddy_struct));

	int buddy_page_num=1<<(order-PAGE_ORDER);

	get_fixed_pages(buddy_page_num);
	// fill the buddy_struct
	buddy_struct->order=order;
	buddy_struct->poolsize=PAGE_SIZE<<(order-PAGE_ORDER);
	buddy_struct->freelist=(void **)((void *)buddy_struct + sizeof(*buddy_struct));
    rt_memset(buddy_struct->freelist,0,sizeof(void *)*buddy_struct->order);
	buddy_struct->free_size=buddy_struct->poolsize;
	buddy_struct->pool=addr;

	// empty the buddy's pool
	rt_memset(buddy_struct->pool,0,buddy_struct->poolsize);
 
	// init the buddy_struct's freelist       
	listarray = buddy_struct->freelist;
	listarray[order]=buddy_struct->pool;

	// return buddy_struct's site
	return buddy_struct;
}

void buddy_clear() 
{

	rt_memset(listarray,0,sizeof(UINT32)*(buddy_struct->order+1));
	listarray[buddy_struct->order]=buddy_struct->pool;
	buddy_struct->free_size=buddy_struct->poolsize;
	return;
}

void * bmalloc(int size) {

	int i, order;
	void * block;
	void * buddymem;
	int alloc_size;

  // calculate minimal order for this size
	i = MIN_ORDER;
	while (BLOCKSIZE(i) < size + 1) // one more byte for storing order
		i++;

	order = i;
	alloc_size=BLOCKSIZE(order);

  // level up until non-null list found
	for (;; i++) {
  		if (i > buddy_struct->order)
			return RT_NULL;
    		if (listarray[i]!=0)
      			break;
  	}

  // remove the block out of list
 	block = listarray[i];
 	listarray[i] = *(void **)listarray[i];

  // split until i == order
 	while (i-- > order) {
		buddymem = buddyof(block, i,buddy_struct);
    		listarray[i] = buddymem;
		*(void * *)buddymem=0;
	}

  // store order in previous byte
 	*((BYTE*) (block - 1)) = order;
	buddy_struct->free_size-=alloc_size;
	return block;
}

void * bmalloc0(int size) 
{
	void *  addr=bmalloc(size);
	if(addr!=0)
	{
		rt_memset(addr,0,size);
	}
	return addr;
}

int TFree(void *  block) {

	int i;
	void * buddymem;
	void * * p;
	int alloc_size;

  // fetch order in previous byte
	i = *((BYTE*) (block - 1));
	alloc_size=BLOCKSIZE(i);

	for (;i<=buddy_struct->order; i++) {
    // calculate buddy
		buddymem = buddyof(block, i,buddy_struct);
		p = &listarray[i];

    // find buddy in list
		while ((*p != RT_NULL) && (*p != buddymem))
			p = (void * *)*p;

    // not found, insert into list
  		if (*p != buddymem) {
  			*(void **)block = listarray[i];
      		listarray[i] = block;
			buddy_struct->free_size+=alloc_size;
      		return 0;
		}
    // found, merged block starts from the lower one
    	block = (block < buddymem) ? block : buddymem;
    // remove buddy out of list
    	*p = *(void **) *p;
  	}
	buddy_struct->free_size+=alloc_size;
	return 0;
}
void * Talloc(int size){
	bmalloc(size);
}
void * Talloc0(int size){
	bmalloc0(size);
}
/*
void bfree0(void * pointer,buddy_t * buddy) 
{
	int i;
	i = *((BYTE*) (pointer - 1));
	if(pointer!=RT_NULL)
	{
		rt_memset(pointer,0,BLOCKSIZE(i));
	}
	bfree(pointer,buddy);
}
*/
/*
 * The following functions are for simple tests.
 */
/*
static int count_blocks(int i,buddy_t * buddy) {

  int count = 0;
  void * * p = &(buddy->freelist[i]);

  while (*p != RT_NULL) {
    count++;
    p = (void **) *p;
  }
  return count;
}

int total_free(buddy_t * buddy) {

  int i, bytecount = 0;

  for (i = MIN_ORDER; i <= buddy->order; i++) {
    bytecount += count_blocks(i,buddy) * BLOCKSIZE(i);
  }
  return bytecount;
}

int ispointerinbuddy(void * pointer,buddy_t * buddy)
{
	int offset=pointer-(void *)buddy->pool;
	if((offset>0) && (offset<buddy->poolsize))
		return 1;
	return 0;
}
*/
/*
static void print_list(int i,buddy_t * buddy) {

  printf("freelist[%d]: \n", i);

  void **p = &buddy->freelist[i];
  while (*p != RT_NULL) {
    printf("    0x%08lx, 0x%08lx\n", (uintptr_t) *p, (uintptr_t) *p - (uintptr_t) buddy->pool);
    p = (void **) *p;
  }
}
*/
