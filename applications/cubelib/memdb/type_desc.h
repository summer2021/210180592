#ifndef TYPE_DESC_H
#define TYPE_DESC_H

struct struct_elem_attr uuid_head_desc[] =
{
	{"uuid",CUBE_TYPE_UUID,DIGEST_SIZE,RT_NULL,RT_NULL},
	{"name",CUBE_TYPE_STRING,DIGEST_SIZE,RT_NULL,RT_NULL},
	{"type",CUBE_TYPE_RECORDTYPE,sizeof(int),RT_NULL,RT_NULL},
	{"subtype",CUBE_TYPE_RECORDSUBTYPE,sizeof(int),RT_NULL,"type"},
	{RT_NULL,CUBE_TYPE_ENDDATA,0,RT_NULL,RT_NULL}
};


struct struct_elem_attr elem_attr_octet_desc[] =
{
	{"name",CUBE_TYPE_ESTRING,sizeof(char *),RT_NULL,RT_NULL},
	{"type",CUBE_TYPE_ELEMTYPE,sizeof(int),RT_NULL,RT_NULL},
	{"size",CUBE_TYPE_INT,sizeof(int),RT_NULL,RT_NULL},
	{"ref",CUBE_TYPE_UUID,DIGEST_SIZE,RT_NULL,RT_NULL},
	{"ref_name",CUBE_TYPE_STRING,DIGEST_SIZE,RT_NULL,RT_NULL},
	{"def",CUBE_TYPE_STRING,DIGEST_SIZE,RT_NULL,RT_NULL},
	{RT_NULL,CUBE_TYPE_ENDDATA,0,RT_NULL}
};

struct struct_elem_attr struct_define_desc[] =
{
	{"elem_no",CUBE_TYPE_INT,sizeof(int),RT_NULL,RT_NULL},
	{"elem_desc",CUBE_TYPE_DEFARRAY,sizeof(void *),&elem_attr_octet_desc,"elem_no"},
	{RT_NULL,CUBE_TYPE_ENDDATA,0,RT_NULL,RT_NULL}
};
struct struct_elem_attr struct_namelist_desc[] =
{
	{"elem_no",CUBE_TYPE_INT,sizeof(int),RT_NULL,RT_NULL},
	{"elemlist",CUBE_TYPE_DEFNAMELIST,sizeof(void *),RT_NULL,"elem_no"},
	{RT_NULL,CUBE_TYPE_ENDDATA,0,RT_NULL,RT_NULL}
};

struct struct_elem_attr struct_typelist_desc[] =
{
	{"elem_no",CUBE_TYPE_INT,sizeof(int),RT_NULL,RT_NULL},
	{"uuid",CUBE_TYPE_UUID,DIGEST_SIZE,RT_NULL,RT_NULL},
	{RT_NULL,CUBE_TYPE_ENDDATA,0,RT_NULL,RT_NULL}
};

struct struct_elem_attr struct_subtypelist_desc[] =
{
	{"type",CUBE_TYPE_RECORDTYPE,sizeof(int),RT_NULL},
	{"elem_no",CUBE_TYPE_INT,sizeof(int),RT_NULL,RT_NULL},
	{"uuid",CUBE_TYPE_UUID,DIGEST_SIZE,RT_NULL,RT_NULL},
	{RT_NULL,CUBE_TYPE_ENDDATA,0,RT_NULL,RT_NULL}
};

struct struct_elem_attr index_list_desc[] =
{
	{"flag",CUBE_TYPE_FLAG,sizeof(int),&elem_attr_flaglist_array,RT_NULL},
	{"elemlist",CUBE_TYPE_ESTRING,sizeof(void *),RT_NULL,RT_NULL},
	{RT_NULL,CUBE_TYPE_ENDDATA,0,RT_NULL,RT_NULL}
};
struct struct_elem_attr struct_recordtype_desc[] =
{
	{"type",CUBE_TYPE_RECORDTYPE,sizeof(int),RT_NULL,RT_NULL},
	{"subtype",CUBE_TYPE_RECORDSUBTYPE,sizeof(int),RT_NULL,"type"},
	{"uuid",CUBE_TYPE_UUID,DIGEST_SIZE,RT_NULL,RT_NULL},
	{"flag_no",CUBE_TYPE_INT,sizeof(int),RT_NULL,RT_NULL},
	{"index",CUBE_TYPE_DEFARRAY,sizeof(void *),&index_list_desc,"flag_no"},
	{RT_NULL,CUBE_TYPE_ENDDATA,0,RT_NULL,RT_NULL}
};
#endif
