本项目基于相关规范实现了sm2算法，具备sm2密钥生成、加密、解密、签名、验签等功能；
本项目从dishibolei的安卓sm2算法库中截取，其代码源于simonpang的gm_sm2_master库。
dishibolei代码原始地址https://github.com/dishibolei/SM2/tree/master/SM2Simple/SM2Simple/GM_SM2-master
simonpang代码原始地址https://github.com/stevenpsm/GM_SM2
本人在dishibolei修改的基础上又做了少量修改，并用标准SM2数据验证了其正确性。
算法实现中调用了libtommath大数库、goldbar的sm3算法；
本项目为标准C实现。
感谢simonpang，dishibilei,libtommath作者及goldbar :)
