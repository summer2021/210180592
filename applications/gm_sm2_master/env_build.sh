#!/bin/bash

cd libtommath-0.42.0
make clean
make
cd -

cd sm2_impl
make clean
cp $CUBE_PATH/cubelib/crypto/sm3_ext.o . 
sleep 1
make
cd -
