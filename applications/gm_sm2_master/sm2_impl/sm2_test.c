
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <rtthread.h>
//#include <dlfcn.h>
//#include <sys/ioctl.h>
#include <string.h>
#include <time.h>
//#include "stdio.h"
//#include "stdlib.h"
#include "sm2.h"
#include "tommath.h"

#define DIGEST_SIZE     32
//#define EINVAL 1
//#define ENOMEM 2
typedef unsigned char BYTE;
typedef unsigned short int UINT16;
typedef unsigned int UINT32;
typedef unsigned long int UINT64;
typedef unsigned short int WORD;
//typedef unsigned int         DWORD;
#define PRIVKEY_LEN 32
#define PUBKEY_LEN 64
BYTE * privkey = RT_NULL;
unsigned long privkey_len = PRIVKEY_LEN;
BYTE * pubkey = RT_NULL;
BYTE ExBuf[DIGEST_SIZE * 32];

int TCM_ExCreateSm2Key(BYTE ** privkey, int * privkey_len, BYTE ** pubkey)
{
    int ret = 0;
    int i;

    printf("Begin ex Create sm2 key:\n");

    BYTE prikey[DIGEST_SIZE * 2];
    BYTE pubkey_XY[64];
    unsigned long prilen = DIGEST_SIZE * 2;

    ret = GM_GenSM2keypair(prikey, &prilen, pubkey_XY);
    if (ret != 0)
        return -EINVAL;
    *privkey_len = prilen;

    *privkey = malloc(prilen);
    if (*privkey == RT_NULL)
        return -ENOMEM;
    memcpy(*privkey, prikey, prilen);
    *pubkey = malloc(64);
    if (*pubkey == RT_NULL)
        return -ENOMEM;
    memcpy(*pubkey, pubkey_XY, 64);
    return 0;
}

int TCM_ExCreateSm2KeywithFile(BYTE **privkey, BYTE **pubkey, const char * privatepath, const char * pubpath)
{
    if (*pubkey == RT_NULL)
    {
        *pubkey = (BYTE*) malloc(PUBKEY_LEN);
    }
    if (*privkey == RT_NULL)
    {
        *privkey = (BYTE*) malloc(PRIVKEY_LEN);
    }
    int privatefd = open(privatepath, O_RDONLY);
    int pubfd = open(pubpath, O_RDONLY);
    if (privatefd < 0 || pubfd < 0)
    {
        printf("open file error!\n");
        exit(-1);
    }
    int a = read(privatefd, *privkey, PRIVKEY_LEN);
    int b = read(pubfd, *pubkey, PUBKEY_LEN);
    printf("%d %d\n", a, b);
    return 0;
}
int sm2Encrypt(BYTE * pubkey, BYTE * output, int * outlen, BYTE * in, int inlen)
{
    if (pubkey == RT_NULL)
    {
        printf("pubkey is empty\n");
        exit(-1);
    }
    int ret = -1;
    *outlen = inlen + 65 + 32 + 4;

    ret = GM_SM2Encrypt(output, outlen, in, inlen, pubkey, strlen(pubkey));
    if (ret != 0)
    {
        printf("sm2 encrypt is failed\n");
        exit(-1);
    }

    return ret;
}

int sm2Decrypt(BYTE * privatekey, BYTE * output, unsigned long *outputlen, BYTE * input, unsigned long inputlen)
{
    if (privatekey == RT_NULL)
    {
        printf("privatekey is empty!\n");
        exit(-1);
    }
    int ret = 1;
    ret = GM_SM2Decrypt(output, outputlen, input, inputlen, privatekey, strlen(privatekey));
    if (ret != 0)
    {
        printf("decrypt error!\n");
        exit(-1);
    }
    return ret;
}
int makerandomstring(int length, BYTE ** string)
{
    srand((unsigned) time(RT_NULL));
    int i = 0;
    BYTE * randomstring = (BYTE*) malloc(sizeof(BYTE) * length);
    for (i = 0; i < length - 1; i++)
    {
        randomstring[i] = rand() % 0xff;
    }
    *string = randomstring;
    return 0;
}
int print_hex_data(int length, BYTE * string)
{
    int i = 0;
    for (i = 0; i < length; i++)
    {
        if (i % 16 == 0)
        {
            printf("\n");
        }
        printf("%02x ", string[i]);
    }
    printf("\n");
    return 0;
}
int makeFile()
{
    const char * prikeypath = "privatekey.key";
    const char * pubkeypath = "pubkey.key";
    int pri = open(prikeypath, O_RDWR | O_CREAT);
    int pub = open(pubkeypath, O_RDWR | O_CREAT);
    if (pri < 0 || pub < 0)
    {
        printf("create error\n");
        exit(-1);
    }
    int haswritePri = write(pri, privkey, privkey_len);
    int haswritepub = write(pub, pubkey, 64);
    if (haswritepub < 0 || haswritePri < 0)
    {
        printf("write error\n");
        exit(-1);
    }
    close(pri);
    close(pub);
    return 0;

}
// 测试sm2 算法生成秘钥对
int testsm2main()
{
    int ret = 1;
    ret = TCM_ExCreateSm2Key(&privkey, &privkey_len, &pubkey);
    if (ret == 0)
    {
        printf("create sm2 key successfully\n");
        printf("privkey:\n");
        print_hex_data(privkey_len, privkey);
        printf("pubkey:\n");
        print_hex_data(64, pubkey);
        printf("length:\n");
        printf("%u\n", privkey_len);
    }
    else
    {
        printf("failed to create sm2 key \n");
        return -EINVAL;
    }
    BYTE * signstring;
    BYTE * signdata;

    BYTE SignBuf[DIGEST_SIZE * 4];
    UINT64 signlen = DIGEST_SIZE * 16;
    BYTE UserID[DIGEST_SIZE];
    unsigned long lenUID = DIGEST_SIZE;
    memset(UserID, 'A', 32);
    makerandomstring(128, &signstring);

    memcpy(ExBuf, signstring, 128);

    ret = GM_SM2Sign(SignBuf, &signlen, ExBuf, 64, UserID, lenUID, privkey, privkey_len);

    printf("now show the sign data:\n");
    print_hex_data(signlen, SignBuf);

    lenUID = DIGEST_SIZE;
    memset(UserID, 'A', 32);
    ret = GM_SM2VerifySig(SignBuf, (UINT64) signlen, ExBuf, (UINT64) 64, UserID, (UINT64) lenUID, pubkey, (UINT64) 64);
    BYTE * instring;
    makerandomstring(128, &instring);
    printf("now show the random data:\n");
    print_hex_data(128, instring);
    BYTE * encryptdata = (BYTE*) malloc(sizeof(BYTE) * (512));
    int encryptdatalength = 128;
    sm2Encrypt(pubkey, encryptdata, &encryptdatalength, instring, 128);
    printf("now show the encrypt data:\n");
    print_hex_data(encryptdatalength, encryptdata);

    BYTE * decryptdata = (BYTE*) malloc(sizeof(BYTE) * 512);
    int outputlen = 512;
    ret = sm2Decrypt(privkey, decryptdata, &outputlen, encryptdata, encryptdatalength);
    printf("now show the decrypt data:\n");
    print_hex_data(outputlen, decryptdata);

    //verifysign
    ret = GM_SM2VerifySig(SignBuf, signlen, ExBuf, 64, UserID, lenUID, pubkey, 64);
    if (ret != 0)
    {
        printf("verify failed\n");
        exit(-1);
    }
    else
        printf("verify success!\n");
    free(decryptdata);
    free(instring);
    free(encryptdata);

    return 0;
}

// 通过公钥私钥文件测试sm2算法
int testsm2withfile()
{
    int ret = 1;
    clock_t start, finish;
    double ping;
    start = clock();
    const char * privatepath = "/sdcard/privatekey.key";
    const char * pubpath = "/sdcard/pubkey.key";
    ret = TCM_ExCreateSm2KeywithFile(&privkey, &pubkey, privatepath, pubpath);
    if (ret == 0)
    {
        printf("create sm2 key by file successfully\n");
        printf("privkey:\n");
        print_hex_data(privkey_len, privkey);
        printf("pubkey:\n");
        print_hex_data(64, pubkey);
        printf("length:\n");
        printf("%u\n", privkey_len);
    }
    else
    {
        printf("failed to create sm2 key \n");
        return -EINVAL;
    }
    BYTE * signstring = RT_NULL;
    BYTE * signdata = RT_NULL;

    BYTE SignBuf[DIGEST_SIZE * 32];
    UINT64 signlen = DIGEST_SIZE * 16;
    BYTE UserID[DIGEST_SIZE];
    unsigned long lenUID = DIGEST_SIZE;
    memset(UserID, 'A', 32);
    makerandomstring(128, &signstring);
    memcpy(ExBuf, signstring, 128);
    ret = GM_SM2Sign(SignBuf, &signlen, ExBuf, 64, UserID, lenUID, privkey, privkey_len);
    printf("now show the sign data:\n");
    print_hex_data(signlen, SignBuf);
    lenUID = DIGEST_SIZE;
    memset(UserID, 'A', 32);
    ret = GM_SM2VerifySig(SignBuf, signlen, ExBuf, 64, UserID, lenUID, pubkey, 64);
    free(signstring);
    if (ret == 0)
    {
        printf("sign is success!\n");
    }
    BYTE * instring = RT_NULL;
    printf("now make random data:\n");
    makerandomstring(128, &instring);
    printf("now show the random data:\n");
    print_hex_data(128, instring);
    BYTE * encryptdata = (BYTE*) malloc(sizeof(BYTE) * (512));
    UINT64 encryptdatalength = 128;
    sm2Encrypt(pubkey, encryptdata, &encryptdatalength, instring, 128);
    printf("now show the encrypt data:\n");
    print_hex_data(encryptdatalength, encryptdata);
    free(instring);

    BYTE * decryptdata = (BYTE*) malloc(sizeof(BYTE) * 512);
    UINT64 outputlen = 512;
    ret = sm2Decrypt(privkey, decryptdata, &outputlen, encryptdata, encryptdatalength);
    printf("now show the decrypt data:\n");
    print_hex_data(outputlen, decryptdata);
    finish = clock();
    ping = (double) (finish - start) / CLOCKS_PER_SEC;
    printf("\n the program finish is %.3lf\n", ping);
    free(decryptdata);
    free(encryptdata);
    return 0;
}

// 测试 Ecc 接口签名和验证测试
int test_Ecc_Intrfs_sig_veri()
{
    printf("\n********\n* Ecc interface signature and verify test\n********\n");
    printf(
            "...you can check the route on \"Public Key Cryptographic Algorithm SM2 Based on Elliptic Curves\" page [57]...\n");
    printf(
            "...to check, please make sure that the sm2lib is a debug version, make it under \'-D_DEBUG\' when compile...\n\n\n");
//  char rand_k[] = "6CB28D99385C175C94F94E934817663FC176D925DD72B727260DBAAE1FB2F96F";
//  char dgst[]   = "B524F552CD82B8B028476E005C377FB19A87E6FC682D48BB5D42E3D9B9EFFE76";
//  char pri_dA[] = "128B2FA8BD433C6C068C8D803DFF79792A519A55171B1B650C23661D15897263";

    char rand_k[] = "F026AD9A7EB94401A800C8D8C3277E69972C7F3778ACE4D537012023EDFB69FF";
    char dgst[] = "3854C463FA3F73783621B1CE4EF83F7C78048AAC79B221FCDD290866CC131174";
    char pri_dA[] = "C242939DDAB6FCC07B6676C07D2DC117EC68A09142C25C008630B9756786162D";
    char p1[] = "FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF";

    mp_int mp_Xg, mp_Yg, mp_rx, mp_ry, mp_p, mp_a, mp_rand_k;
    mp_int mp_r, mp_s, mp_dgst, mp_Pri_dA, mp_n;
    mp_int mp_XA, mp_YA;

    int ret = 0;
    mp_init_multi(&mp_Xg, &mp_Yg, &mp_rx, &mp_ry, &mp_p, &mp_a, &mp_rand_k, &mp_r, &mp_s, &mp_dgst, &mp_Pri_dA, &mp_n,
            &mp_XA, &mp_YA, RT_NULL);

    ret = mp_read_radix(&mp_Xg, (char *) Xg, 16);
    CHECK_RET(ret);
    ret = mp_read_radix(&mp_Yg, (char *) Yg, 16);
    CHECK_RET(ret);
//  ret = mp_read_radix(&mp_p, (char *) param_p, 16);
    ret = mp_read_radix(&mp_p, (char *) p1, 16);
    CHECK_RET(ret);
    ret = mp_read_radix(&mp_a, (char *) param_a, 16);
    CHECK_RET(ret);
    ret = mp_read_radix(&mp_n, (char *) param_n, 16);
    CHECK_RET(ret);
    ret = mp_read_radix(&mp_dgst, (char *) dgst, 16);
    CHECK_RET(ret);
    ret = mp_read_radix(&mp_Pri_dA, (char *) pri_dA, 16);
    CHECK_RET(ret);
    ret = mp_read_radix(&mp_rand_k, (char *) rand_k, 16);
    CHECK_RET(ret);

    printf("...params are...\n");
    printf("p=");
    MP_print(&mp_p);
    printf("a=");
    MP_print(&mp_a);
    printf("n=");
    MP_print(&mp_n);
    printf("Xg=");
    MP_print(&mp_Xg);
    printf("Yg=");
    MP_print(&mp_Yg);
    printf("dA=");
    MP_print(&mp_Pri_dA);
    printf("rand=");
    MP_print(&mp_rand_k);

    ret = Ecc_Sm2_sign(&mp_r, &mp_s, &mp_dgst, &mp_rand_k, &mp_Pri_dA, &mp_Xg, &mp_Yg, &mp_a, &mp_p, &mp_n);
    if (ret == 0)
    {
        printf("...signature ok...\n");
    }
    else
    {
        printf("...signature failed!\n");
        CHECK_RET(ret);
    }

    printf("...signature data:\n");
    printf("r=");
    MP_print(&mp_r);
    printf("s=");
    MP_print(&mp_s);

    // compute public key
    ret = Ecc_points_mul(&mp_XA, &mp_YA, &mp_Xg, &mp_Yg, &mp_Pri_dA, &mp_a, &mp_p);
    CHECK_RET(ret);

    printf("...public key:\n");
    printf("XA=");
    MP_print(&mp_XA);
    printf("YA=");
    MP_print(&mp_YA);

    printf("......verify signature...\n");
    ret = Ecc_Sm2_verifySig(&mp_r, &mp_s, &mp_dgst, &mp_XA, &mp_YA, &mp_Xg, &mp_Yg, &mp_a, &mp_p, &mp_n);
    if (ret == 0)
    {
        printf("\nverify ok!\n");
    }
    else
    {
        printf("\nverify failed!\n");
        CHECK_RET(ret);
    }

    END: mp_clear_multi(&mp_Xg, &mp_Yg, &mp_rx, &mp_ry, &mp_p, &mp_a, &mp_rand_k, &mp_r, &mp_s, &mp_dgst, &mp_Pri_dA,
            &mp_n, &mp_XA, &mp_YA, RT_NULL);
    printf("********\n* test end\n********\n");
    return ret;
}

// 测试 GM算法 sm2 加密 解密
int test_GM2_encryption_and_decryption()
{
    printf("\n********\n* GM sm2 asym encryption and decryption test\n********\n");
    printf(
            "...you can check the route on \"Public Key Cryptographic Algorithm SM2 Based on Elliptic Curves\" page [90]...\n");
    printf(
            "...to check, please make sure that the sm2lib is a debug version, make it under \'-D_DEBUG\' when compile...\n\n\n");
    unsigned char buff[64] = { 0 };
    unsigned long buffLen = 64;
    unsigned char prikeyBuff[200] = { 0 };
    unsigned long priLen = 200;
    int ret = 0;
    char plain[] = "67";
    unsigned char encData[1000] = { 0 };
    unsigned long encLen = 1000;
    unsigned char decData[1000] = { 0 };
    unsigned long decLen = 1000;

    char pubkey_B_XY[] =
            "435B39CCA8F3B508C1488AFC67BE491A0F7BA07E581A0E4849A5CF70628A7E0A75DDBA78F15FEECB4C7895E2C1CDF5FE01DEBB2CDBADF45399CCF77BBA076A42";
    char prikey[] = "1649AB77A00637BD5E2EFE283FBF353534AA7F7CB89463F208DDBC2920BB0DA0";
    ret = hexCharStr2unsignedCharStr(pubkey_B_XY, strlen(pubkey_B_XY), 0, buff, &buffLen);
    CHECK_RET(ret);
    ret = hexCharStr2unsignedCharStr(prikey, strlen(prikey), 0, prikeyBuff, &priLen);
    CHECK_RET(ret);

    printf("...public key\n");
    printf("XB=");
    BYTE_print(buff, 32);
    printf("YB=");
    BYTE_print(buff + 32, 32);

    printf("...plain text:\n%s\n", plain);
    ret = GM_SM2Encrypt(encData, &encLen, (unsigned char *) plain, strlen(plain), buff, buffLen);
    CHECK_RET(ret);
    ret = GM_SM2Decrypt(decData, &decLen, encData, encLen, prikeyBuff, priLen);
    CHECK_RET(ret);
    printf("\n...decdata:%s\n", decData);
    END:

    printf("********\n* test end\n********\n");
    return ret;
}

// 测试 GM算法 sm2 生成密钥对
int test_gen_SM2_GM_keypair()
{
    printf("\n********\n* GM sm2 keypair generation test\n********\n\n");

    unsigned char buff[64] = { 0 };
    unsigned long buffLen = 64;
    unsigned char prikeyBuff[200] = { 0 };
    unsigned long priLen = 200;
    int ret = 0;
    ret = GM_GenSM2keypair(prikeyBuff, &priLen, buff);
    CHECK_RET(ret);

    printf("...pubkey (XA,YA):\n");
    printf("XA=");
    BYTE_print(buff, 32);
    printf("YA=");
    BYTE_print(buff + 32, 32);
    printf("...prikey dA:\n");
    BYTE_print(prikeyBuff, priLen);

    END: printf("********\n* test end\n********\n");
    return ret;

}

MSH_CMD_EXPORT(testsm2main, testsm2main)
MSH_CMD_EXPORT(testsm2withfile, testsm2withfile)
MSH_CMD_EXPORT(test_Ecc_Intrfs_sig_veri, test_Ecc_Intrfs_sig_veri)
MSH_CMD_EXPORT(test_GM2_encryption_and_decryption, test_GM2_encryption_and_decryption)
MSH_CMD_EXPORT(test_gen_SM2_GM_keypair, test_gen_SM2_GM_keypair)
