//
// Created by zhaoyang on 2021/3/11.
//

/*#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dlfcn.h>
#include <sys/ioctl.h>
#include <string.h>
#include <time.h>
#define DIGEST_SIZE     32
#define EINVAL 1
#define ENOMEM 2
typedef unsigned char         BYTE;
typedef unsigned short int  UINT16;
typedef unsigned int        UINT32;
typedef unsigned long int   UINT64;
typedef unsigned short int    WORD;
typedef unsigned int         DWORD;
#define PRIVKEY_LEN 32
#define PUBKEY_LEN 64
BYTE * privkey = RT_NULL;
unsigned long  privkey_len =PRIVKEY_LEN;
BYTE * pubkey = RT_NULL;
BYTE ExBuf[DIGEST_SIZE*32];

int TCM_ExCreateSm2Key(BYTE ** privkey,int * privkey_len,BYTE ** pubkey)
{
    int ret=0;
    int i;

    printf("Begin ex Create sm2 key:\n");

    BYTE prikey[DIGEST_SIZE*2];
    BYTE pubkey_XY[64];
    unsigned long prilen=DIGEST_SIZE*2;

    ret=GM_GenSM2keypair(prikey,&prilen,pubkey_XY);
    if(ret!=0)
        return -EINVAL;
    *privkey_len=prilen;

    *privkey=malloc(prilen);
    if(*privkey==RT_NULL)
        return -ENOMEM;
    memcpy(*privkey,prikey,prilen);
    *pubkey=malloc(64);
    if(*pubkey==RT_NULL)
        return -ENOMEM;
    memcpy(*pubkey,pubkey_XY,64);
    makeFile();
    return 0;
}
int TCM_ExCreateSm2KeywithFile(BYTE **privkey,BYTE **pubkey,const char * privatepath,const char * pubpath)
{
	if(*pubkey ==RT_NULL)
	{	
		*pubkey=(BYTE*)malloc(PUBKEY_LEN);
	}
	if(*privkey==RT_NULL)
	{
		*privkey=(BYTE*)malloc(PRIVKEY_LEN);
	}
	int privatefd = open(privatepath,O_RDONLY);
	int pubfd = open(pubpath,O_RDONLY);
	if(privatefd<0 || pubfd<0){
		printf("open file error!\n");
		exit(-1);
	}
	int a =read(privatefd,*privkey,PRIVKEY_LEN);
	int b =read(pubfd,*pubkey,PUBKEY_LEN);
	printf("%d %d\n",a,b);		
	return 0;	
}
int sm2Encrypt(BYTE * pubkey,BYTE * output,int * outlen,BYTE * in, int inlen)
{
	if(pubkey==RT_NULL){
		printf("pubkey is empty\n");
		exit(-1);
	}
	int ret = -1;
	*outlen = inlen + 65 + 32 + 4;
	
	ret = GM_SM2Encrypt(output,outlen,in,inlen,pubkey,strlen(pubkey));
	if(ret!=0){
		printf("sm2 encrypt is failed\n");
		exit(-1);
	}
	
	return ret;
}

int sm2Decrypt(BYTE * privatekey,BYTE * output,unsigned long *outputlen,BYTE * input,unsigned long inputlen)
{
    if(privatekey==RT_NULL){
        printf("privatekey is empty!\n");
        exit(-1);
    }
    int ret=1;
    ret = GM_SM2Decrypt(output,outputlen,input,inputlen,privatekey,strlen(privatekey));
    if(ret!=0){
         printf("decrypt error!\n");
         exit(-1);
    } 
    return ret;
}
int makerandomstring(int length,BYTE ** string)
{	
	srand((unsigned)time(RT_NULL));
	int i = 0;
	BYTE * randomstring = (BYTE*)malloc(sizeof(BYTE)*length);
	for(i = 0 ; i < length-1 ;i++){
		randomstring[i]=rand()%0xff;			
	}
    *string=randomstring;
	return 0;
}
int print_hex_data(int length,BYTE * string)
{
	int i = 0;
	for(i = 0 ; i<length;i++)
	{
		if(i%16==0){
			printf("\n");
		}
		printf("%02x ",string[i]);	
	}
	printf("\n");
	return 0;
}
int makeFile()
{
	const char * prikeypath="privatekey.key";
	const char * pubkeypath="pubkey.key";
	int pri = open(prikeypath,O_RDWR|O_CREAT);
	int pub = open(pubkeypath,O_RDWR|O_CREAT);
	if(pri<0 || pub<0){
		printf("create error\n");
		exit(-1);
	}
	int haswritePri= write(pri,privkey,privkey_len);
	int haswritepub= write(pub,pubkey,64);
	if(haswritepub<0 || haswritePri<0){
		printf("write error\n");
		exit(-1);
	}
	close(pri);
	close(pub);
	return 0;
			
}
int main()
{
    int ret = 1;
 //   ret =TCM_ExCreateSm2Key(&privkey, &privkey_len, &pubkey);
    const char *privatepath="privatekey.key";
    const char *pubpath="pubkey.key";
    ret = TCM_ExCreateSm2KeywithFile(&privkey,&pubkey,privatepath,pubpath); 
    if(ret ==0){
        printf("create sm2 key successfully\n");
	printf("privkey:\n");
	print_hex_data(32,privkey);
	printf("pubkey:\n");
	print_hex_data(64,pubkey);
    }
    else{
        printf("failed to create sm2 key \n");
        return -EINVAL;
    }
    BYTE * signstring;
    BYTE * signdata;

	BYTE SignBuf[DIGEST_SIZE*4];
    UINT64 signlen=DIGEST_SIZE*16;
   	BYTE UserID[DIGEST_SIZE];
   	unsigned long lenUID = DIGEST_SIZE;
   	memset(UserID, 'A', 32);
    makerandomstring(128,&signstring);

    memcpy(ExBuf,signstring,128);


	ret=GM_SM2Sign(SignBuf,&signlen,
		ExBuf,64,
		UserID,lenUID,
		privkey,privkey_len);	

    printf("now show the sign data:\n");
    print_hex_data(signlen,SignBuf);
    lenUID = DIGEST_SIZE;
    memset(UserID, 'A', 32);
    BYTE * instring ;
    makerandomstring(128,&instring);
    printf("now show the random data:\n");
    print_hex_data(128,instring);
    BYTE * encryptdata = (BYTE*)malloc(sizeof(BYTE)*(512));
    int  encryptdatalength = 128;
    sm2Encrypt(pubkey,encryptdata,&encryptdatalength,instring,128);
    printf("now show the encrypt data:\n");
    print_hex_data(encryptdatalength,encryptdata);

    BYTE * decryptdata=(BYTE*)malloc(sizeof(BYTE)*512);
    int outputlen = 512;
    ret = sm2Decrypt(privkey,decryptdata,&outputlen,encryptdata,encryptdatalength);
    printf("now show the decrypt data:\n");
    print_hex_data(outputlen,decryptdata);

    //verifysign
    ret=GM_SM2VerifySig(SignBuf,(UINT64)signlen,ExBuf,(UINT64)64,UserID,(UINT64)lenUID,pubkey,(UINT64)64);
    if(ret!=0){
	printf("verify failed\n");
	exit(-1);
    }
    else
	printf("verify success!\n");
    free(decryptdata);
    free(instring);
    free(encryptdata);
    

    return 0;
}*/
